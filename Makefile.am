ACLOCAL_AMFLAGS=-I m4

AM_CFLAGS=-pthread -ffast-math -std=c11 -Wall -Wextra -Isrc -I$(top_srcdir)/src

bin_PROGRAMS = super

super_SOURCES = src/main.c \
		src/argtable3.c \
		src/arguments.c
super_LDADD = libsuper.la

lib_LTLIBRARIES = libsuper.la

libsuper_la_SOURCES = 	src/vector_set.c             \
			src/database.c               \
			src/serrors.c                \
			src/matrix.c                 \
			src/util.c                   \
			src/super_runner.c           \
			src/query_parser.c           \
			src/fast_bounds_check.c      \
			src/result.c                 \
			src/numeric.c

libsuper_la_LDFLAGS = -version-info 0:0:0

HEADER_FILES = 	src/argtable3.h			\
		src/arguments.h			\
		src/vector_set.h		\
		src/database.h			\
		src/serrors.h			\
		src/matrix.h			\
		src/util.h			\
		src/super_runner.h		\
		src/query_parser.h		\
		src/fast_bounds_check.h		\
		src/result.h			\
		src/numeric.h

SUPERDATABASE_MODULE = 				\
		superdatabase/__init__.py	\
		superdatabase/application.py	\
		superdatabase/db.py		\
		superdb.in

TEST_FILES = 	test/data/invalid.database	\
		test/data/test_split.db		\
		test/data/test_whole.db		\
		test/data/ts1data.db		\
		test/data/ts1qry.db		\
		test/data/ts2data.db		\
		test/data/ts2qry.db		\
		test/data/ts3data.db		\
		test/data/ts3qry.db		\
		test/data/ts4data.db		\
		test/data/ts4qry.db		\
		test/data/ts5data.db		\
		test/data/ts5qry.db		\
		test/data/ts6data.db		\
		test/data/ts6qry.db		\
		test/data/ts7data.db		\
		test/data/ts7qry.db		\
		test/data/ts8data.db		\
		test/data/ts8qry.db		\
		test/data/ts9data.db		\
		test/data/ts9qry.db		\
		test/data/2ic7_query.pdb	\
		test/data/example_database.db

EXTRA_DIST =	m4/ax_code_coverage.m4		\
		m4/ax_pthread.m4		\
		$(HEADER_FILES)			\
		$(SUPERDATABASE_MODULE)		\
		$(TEST_FILES)

install-exec-hook:
	$(mkinstalldirs) $(DESTDIR)/$(bindir)/superdatabase
	$(INSTALL) $(srcdir)/superdb $(DESTDIR)/$(bindir)/superdb
	$(INSTALL) $(srcdir)/superdatabase/application.py $(DESTDIR)/$(bindir)/superdatabase/
	$(INSTALL) $(srcdir)/superdatabase/db.py $(DESTDIR)/$(bindir)/superdatabase/

uninstall-hook:
	rm -rf $(DESTDIR)/$(bindir)/superdatabase/
	rm -rf $(DESTDIR)/$(bindir)/superdb

SCRIPTS_FOR_TESTING = test/single-query-1 	\
	test/gapped-query-1 			\
	test/convert-database-1

PROGRAMS_FOR_TESTING = test_database		\
	test_query_parser			\
	test_vector_set				\
	test_matrix				\
	test_bounds_check			\
	test_result				\
	test_util

TESTS = $(PROGRAMS_FOR_TESTING)			\
	test/python_unit_tests			\
	$(SCRIPTS_FOR_TESTING)

EXTRA_DIST += $(SCRIPTS_FOR_TESTING)

check_PROGRAMS = $(PROGRAMS_FOR_TESTING)
test_database_SOURCES = src/database.c 		\
	test/test_database.c
test_database_CFLAGS=$(AM_CFLAGS) @CODE_COVERAGE_CFLAGS@ -DTEST_DATA_PATH=""\""./$(VPATH)/test/data"\"""
test_database_LDADD=@CHECK_LIBS@ @CODE_COVERAGE_LDFLAGS@

test_query_parser_SOURCES = src/query_parser.c	\
	src/database.c 				\
	src/numeric.c			 	\
	test/test_query_parser.c
test_query_parser_CFLAGS=$(AM_CFLAGS) @CODE_COVERAGE_CFLAGS@ -DTEST_DATA_PATH=""\""./$(VPATH)/test/data"\"""
test_query_parser_LDADD=@CHECK_LIBS@ @CODE_COVERAGE_LDFLAGS@

test_vector_set_SOURCES = src/vector_set.c	\
	src/database.c				\
	src/numeric.c				\
	test/test_vector_set.c
test_vector_set_CFLAGS=$(AM_CFLAGS) @CODE_COVERAGE_CFLAGS@ -DTEST_DATA_PATH=""\""./$(VPATH)/test/data"\"""
test_vector_set_LDADD=@CHECK_LIBS@ @CODE_COVERAGE_LDFLAGS@

test_matrix_SOURCES = src/matrix.c		\
	src/database.c				\
	src/vector_set.c			\
	src/query_parser.c			\
	src/numeric.c				\
	test/test_matrix.c
test_matrix_CFLAGS=$(AM_CFLAGS) @CODE_COVERAGE_CFLAGS@ -DTEST_DATA_PATH=""\""./$(VPATH)/test/data"\"""
test_matrix_LDADD=@CHECK_LIBS@ @CODE_COVERAGE_LDFLAGS@

test_bounds_check_SOURCES = 			\
	src/numeric.c				\
	src/fast_bounds_check.c			\
	src/database.c				\
	src/vector_set.c			\
	src/query_parser.c			\
	test/test_bounds_check.c
test_bounds_check_CFLAGS=$(AM_CFLAGS) @CODE_COVERAGE_CFLAGS@ -DTEST_DATA_PATH=""\""./$(VPATH)/test/data"\"""
test_bounds_check_LDADD=@CHECK_LIBS@ @CODE_COVERAGE_LDFLAGS@

test_result_SOURCES = 				\
	src/result.c				\
	src/database.c				\
	src/vector_set.c			\
	src/query_parser.c			\
	src/numeric.c				\
	test/test_result.c
test_result_CFLAGS=$(AM_CFLAGS) @CODE_COVERAGE_CFLAGS@ -DTEST_DATA_PATH=""\""./$(VPATH)/test/data"\"""
test_result_LDADD=@CHECK_LIBS@ @CODE_COVERAGE_LDFLAGS@

test_util_SOURCES = src/util.c 			\
	test/test_util.c
test_util_CFLAGS=$(AM_CFLAGS) @CODE_COVERAGE_CFLAGS@ -DTEST_DATA_PATH=""\""./$(VPATH)/test/data"\"""
test_util_LDADD=@CHECK_LIBS@ @CODE_COVERAGE_LDFLAGS@

CODE_COVERAGE_IGNORE_PATTERN="*test/*" "*src/argtable.c" "*sys/stat.h" "*/ctype.h"
@CODE_COVERAGE_RULES@

local-code-coverage-output: code-coverage-capture
	- cat super-$(VERSION)-coverage/index.html|grep "headerCovTableEntry\(Hi\|Med\|Lo\)"|head -1|sed 's/^.*>\([0-9]\+\.[0-9]\+\s*%\)<.*$$/ coverage lines: \1/' || true

lint:
	- @C_LINT_PROGRAM@ $(top_srcdir)/src/
	- @PY_LINT_PROGRAM@ -r no $(top_srcdir)/superdb $(top_srcdir)/superdatabase || exit 0

check-local: lint

.PHONY: lint local-code-coverage-output
