#!/usr/bin/python
# This file is part of Super. A 3D pattern-matching program.
#
# Copyright 2017 James Collier <james.collier412@gmail.com>
#
# Super is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Super is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Super.  If not, see <http://www.gnu.org/licenses/>.

"""Python interface to a Super Database
"""

import struct

VERSION_0_MAGIC = 0xECEEDE00
VERSION_1_MAGIC = 0xEDABAEEC

ONE_LETTER_CODE \
    = {'ALA':'A'.encode('utf-8'), 'ARG':'R'.encode('utf-8'),
       'ASN':'N'.encode('utf-8'), 'ASP':'D'.encode('utf-8'),
       'CYS':'C'.encode('utf-8'), 'GLU':'E'.encode('utf-8'),
       'GLN':'Q'.encode('utf-8'), 'GLY':'G'.encode('utf-8'),
       'HIS':'H'.encode('utf-8'), 'ILE':'I'.encode('utf-8'),
       'LEU':'L'.encode('utf-8'), 'LYS':'K'.encode('utf-8'),
       'MET':'M'.encode('utf-8'), 'PHE':'F'.encode('utf-8'),
       'PRO':'P'.encode('utf-8'), 'SER':'S'.encode('utf-8'),
       'THR':'T'.encode('utf-8'), 'TRP':'W'.encode('utf-8'),
       'TYR':'Y'.encode('utf-8'), 'VAL':'V'.encode('utf-8'),
       'SEC':'U'.encode('utf-8'), 'PYL':'O'.encode('utf-8'),
       'ASX':'B'.encode('utf-8'), 'GLX':'Z'.encode('utf-8'),
       'XLE':'J'.encode('utf-8'), 'XAA':'X'.encode('utf-8')}

class SuperDatabase(object):
    """Interface to a Super Database
    """
    def __init__(self):
        self.header = {'magic': VERSION_1_MAGIC, 'version': 1,
                       'entry_count': 0, 'metadata': 0}
        self.entry_headers = []

    def entries(self):
        """A list of entry identifiers: STRUCTURE ID - CHAIN ID"""
        return [e['name'].decode('utf-8')+'-'+ e['chain'].decode('utf-8') \
                + '\t[' + str(e['size']) + ']' for e in self.entry_headers]

    def load_from_files(self, filelist):
        """Yeilds a database entry consisting of: COORDINATES, META
           Headers are written to self.entry_headers
        """
        import prody
        meta_index = 0
        for filename in filelist:
            prot, header = prody.parsePDB(filename, subset='calpha',
                                          model=1, header=True)
            eid = header['identifier'].ljust(4)[0:4].encode('utf-8')
            for chain in prot.getHierView():
                self.entry_headers \
                    .append({'name': eid,
                             'chain': chain.getChid().encode('utf-8'),
                             'size': len(chain), 'metaIndex': meta_index})
                meta_index += len(chain)
                yield [list(res.getAtom('CA').getCoords()) for res in chain], \
                    [(res.getResnum(),
                      ONE_LETTER_CODE.get(res.getResname(),
                                          'X'.encode('utf-8')),
                      '_'.encode('utf-8')) \
                     if len(res.getIcode()) == 0 else \
                     (res.getResnum(),
                      ONE_LETTER_CODE.get(res.getResname(),
                                          'X'.encode('utf-8')),
                      res.getIcode().encode('utf-8')) \
                     for res in chain]

    def load_old_version(self, filename):
        """Load an older version (version 0) Super database
        """
        with open(filename, mode='rb') as data:
            offset = 12
            content = data.read(12)
            magic, _, self.header['entry_count'] = struct.unpack("IHI", content)
            if magic != VERSION_0_MAGIC:
                raise ValueError("Bad Version 0 Magic")
            data.seek(offset)
            meta_index = 0
            #entry header size is 9 bytes
            i = 0
            while i < self.header['entry_count']:
                content = data.read(9)
                offset += 9
                data.seek(offset)
                entry = {'name': '', 'chain': '', 'size': 0,
                         'metaIndex': meta_index}
                entry['size'], entry['name'], \
                    entry['chain'] = struct.unpack("I4sc", content)
                self.entry_headers.append(entry)
                meta_index += entry['size']
                coordinates = []
                meta = []
                j = 0
                while j < entry['size']:
                    content = data.read(28)
                    offset += 28
                    data.seek(offset)
                    cdata = struct.unpack("dddhcc", content)
                    coordinates.append(cdata[:3])
                    meta.append((cdata[3], cdata[5], cdata[4]))
                    j += 1
                yield coordinates, meta
                i += 1

    def write(self, filename, data_generator):
        """Write a new Super database file

        Accepts coordinate entry and metadata from data_generator
        Writes data to filename
        """
        from shutil import copyfileobj
        from os import remove
        meta_offset = 16 #sizeof database header
        extfile = filename + ".meta"
        with open(filename, mode='wb') as cdt, \
             open(extfile, mode='w+b') as ext:
            cdt.write(struct.pack("<IIII", self.header['magic'],
                                  self.header['version'],
                                  self.header['entry_count'],
                                  self.header['metadata']))
            e_count = 0
            # Loop over entries
            for coordinates, meta in data_generator:
                cdt.write(struct.pack("<4sIHcx",
                                      self.entry_headers[e_count]['name'],
                                      self.entry_headers[e_count]['metaIndex'],
                                      self.entry_headers[e_count]['size'],
                                      self.entry_headers[e_count]['chain']))
                e_count += 1
                meta_offset += 12 #sizeof entry header
                meta_offset += (12 * len(coordinates)) #sizeof coordinate data
                for crd in coordinates:
                    cdt.write(struct.pack("<fff", crd[0], crd[1], crd[2]))

                for mdata in meta:
                    ext.write(struct.pack("<hcc", mdata[0], mdata[1], mdata[2]))

            self.header['entry_count'] = e_count
            self.header['metadata'] = meta_offset
            ext.seek(0)
            copyfileobj(ext, cdt)

            cdt.seek(0)
            cdt.write(struct.pack("<IIII", self.header['magic'],
                                  self.header['version'],
                                  self.header['entry_count'],
                                  self.header['metadata']))
        remove(extfile)

    def read(self, filename):
        """Read a (version 1) Super database
        """
        with open(filename, mode='rb') as v1db:
            content = v1db.read()
            self.header['magic'], self.header['version'], \
                self.header['entry_count'], self.header['metadata'] \
                = struct.unpack("<IIII", content[0:16])
            if self.header['magic'] != VERSION_1_MAGIC:
                raise ValueError("Bad Version 1 Magic")
            elif self.header['version'] != 1:
                raise ValueError("Incorrect version number")
            offset = 16
            for i in range(self.header['entry_count']):
                self.entry_headers.append({})
                self.entry_headers[i]['name'], \
                    self.entry_headers[i]['metaIndex'], \
                    self.entry_headers[i]['size'], \
                    self.entry_headers[i]['chain'] \
                    = struct.unpack("<4sIHcx", content[offset: offset+12])
                offset += 12
                offset += (self.entry_headers[i]['size'] * 12)
