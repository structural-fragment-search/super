# Super: A 3D object search program
# SuperDB: A database administration tool for Super
# Copyright (c) 2017, James Collier <james.collier412@gmail.com>
#
# Super is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Super is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Super.  If not, see <http://www.gnu.org/licenses/>.

"""Super database administration tool main application class
"""

import sys
import os
import readline
import cmd
from superdatabase.db import SuperDatabase

class SuperDBApp(cmd.Cmd):
    """Main Super database administration tool application class
    """

    def __init__(self, dbfile, stdin=sys.stdin, stdout=sys.stdout):
        cmd.Cmd.__init__(self, 'tab', stdin, stdout)
        self.database = SuperDatabase()
        self.target = dbfile
        self.prompt = 'superdb> '

    def do_load(self, arg):
        'Load the database'
        if os.path.isfile(arg):
            self.target = arg
            self.database = SuperDatabase()
            self.database.read(self.target)
            print("Success")
        elif os.path.isfile(self.target):
            self.database.read(self.target)
            print("Success")
        else:
            print("No database to load")

    def do_EOF(self, arg):
        'Quit the shell on an EOF'
        return self.do_quit(arg)

    def do_quit(self, arg):
        'Exit the shell'
        del arg
        print('Bye')
        return True

    def do_create(self, arg):
        'Create a new database from arg, loads database automatically'
        self.create(arg.split())

    def do_convert(self, arg):
        'Convert an older format database to the current format'
        self.convert(arg.split()[0])

    def do_show(self, arg):
        'Show infomation about the database'
        switcher = {
            "db" : (lambda self, lst: self.target+'\t[' + \
                    str(self.database.header['entry_count']) + ']'),
            "entry" : (lambda self, lst:
                       self.database.entry_headers[int(lst[0])]),
            "entries" : (lambda self, lst: '\n'.join(self.database.entries())),
        }
        arguments = arg.split()
        try:
            print(switcher.get(arguments[0])(self, arguments[1:]))
        except KeyError:
            print("Incorrect argument supplied. Expect one of: '",
                  ','.join(list(switcher.keys())), "'")
        except IndexError:
            print("Unknown entry index: "+arguments[1])

    def create(self, infiles):
        """Create a Super database from a list of PDB files
        """
        infile_paths = []
        for infile in infiles:
            if os.path.isfile(infile):
                infile_paths.append(infile)
            elif os.path.isdir(infile):
                for root, _, files in os.walk(infile):
                    for fname in files:
                        infile_paths.append(os.path.join(root, fname))
        data = self.database.load_from_files(infile_paths)
        self.database.write(self.target, data)

    def convert(self, infile):
        """Convert a version 0 database into a version 1 database
        """
        data = self.database.load_old_version(infile)
        self.database.write(self.target, data)
