/* This file is part of Super. A 3D pattern-matching program.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file numeric.h
 * \brief Numerical support routines for mathematical functions
 */

#include <math.h>

#include <database.h>

/**
 * \brief Compute the L2 (euclidean) norm of a 3D vector
 */
float
vector_norm(const struct db_vector *restrict const v);

/**
 * \brief Compute the L2 norms for a range of vectors
 */
void
compute_vector_norms(const struct db_vector *restrict vec,
		     float *restrict norm, uint32_t num);
