/* This file is part of Super. A 3D pattern-matching program.
 * Command line arguments
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARGUMENTS_H__
#define __ARGUMENTS_H__

#include <stdio.h> /* FILE */

/**
 * \file arguments.h
 * \brief Process command line arguments into a usable form (interface)
 */

/**
 * \brief Structure to store command line arguments
 */
struct arguments{
  int verbose;           // Verbose output flag
  unsigned threads;      // Number of threads to run
  unsigned coil;         // Size of the gap between query segments
  unsigned n_query;      // Number of query segments
  char **query;          // Query segment filenames
  char *search_database; // Database filename
  FILE *output;          // Output stream
  float threshold;       // Search threshold
};

/**
 * \brief Parse input arguments into the supplied arguments structure
 * \param argc Raw argument count passed to main()
 * \param argv Raw argument vector passed to main()
 * \param arguments A pre-allocated arguments structure to be filled
 */
void
parse_arguments(int argc, char** argv, struct arguments* arguments);

/**
 * \brief Free resources used when filling an arguments structure using
 * parse_arguments()
 * The structure itself is not free()'d, that's the responsibility of the user
 * \param arguments An arguments structure to emptry
 */
void
free_arguments_internal(struct arguments* arguments);

#endif /* __ARGUMENTS_H__ */
