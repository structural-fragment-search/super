/* This file is part of Super. A 3D pattern-matching program.
 * Store and print out match results.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <result.h>

/**
 * \file result.c
 * \brief Storage and output of query match results
 */

/**
 * \brief Internal recursive free of a result list
 */
static void
_result_list_destroy(struct result* list);

void
result_list_append(struct result* list, struct result* new_result)
{
  assert(list != NULL);
  assert(new_result != NULL);
  struct result* node = list;
  while(node->next){
    node = node->next;
  }
  node->next = new_result;
}

int
result_list_print(struct result *restrict list,
                  const struct db *restrict db,
                  const struct query_data *restrict query,
                  const struct arguments *restrict args)
{
  if(list == NULL || db == NULL || query == NULL || args == NULL) return EMEM;
  for(const struct result* node = list; node; node = node->next){
    const struct db_meta* meta = db_meta_by_index(db, node->meta_offset);
    fprintf(args->output, "%.4s\t%c\t%4u%c:%u%c\t%.3f\t",
            node->ID, node->chain, meta->residueID, meta->icode,
            meta[node->size - 1].residueID, meta[node->size - 1].icode,
            node->rmsd);
    for(uint32_t i = 0; i < node->size; i++){
      fputc(meta[i].slc, args->output);
    }
    fputc(':', args->output);
    if(args->coil > 0){
      fprintf(args->output, "%.*s", (int)query->divider, query->sequence);
      for(uint32_t i = 0; i < args->coil; i++){
        fputc('-', args->output);
      }
    }
    fprintf(args->output, "%s\n", &query->sequence[query->divider]);
  }

  return ENONE;
}

void
result_list_destroy(struct result** list)
{
  if(list == NULL) return;
  if(*list == NULL) return;
  _result_list_destroy(*list);
  *list = NULL;
  return;
}

static void
_result_list_destroy(struct result* list)
{
  assert(list != NULL);
  if(list->next == NULL){
    free(list);
    return;
  }else{
    _result_list_destroy(list->next);
    free(list);
    return;
  }
}
