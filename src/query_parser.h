/* This file is part of Super. A 3D pattern-matching program.
 * Interface to a subset of Brookhaven PDB file data to create
 * a query vector set
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __QUERY_PARSER_H__
#define __QUERY_PARSER_H__

#include <database.h>

/**
 * \file query_parser.h
 * \brief Interface to load and manipulate query coordinate data
 */

/**
 * \struct query_data
 * Stores query coordinate- and meta-data
 */
struct query_data {
  struct db_vector *centered; // Centered coordinate data
  float *norm;                // Vector norms
  char *sequence;             // Amino-acid sequence (single-letter-codes)
  unsigned size;              // Number of coordinate vectors in the query
  unsigned divider;           // Location of a split/gap
};

/**
 * \brief Parse a set of PDB format files into a query_data structure
 * \param query A pre-allocated structure to be filled by this function
 * \param filename A list of filenames to be parsed
 * \param num The number of filenames (queries to parse)
 * \return EMEM if \a query is not allocated, or an internal allocation fails
 * \return EINVALID if query is too small or a query file is not a regular file
 * \return EEXIST if a query filename does not exist
 * \return EOOB if a query file seems too large
 * \return EUNKNOWN is a file fails to open
 * \return ENONE on success
 * \bug This is a long and complex function. It needs to be made clearer
 *      and simpler
 */
int
query_parse(struct query_data* query, const char** filename, unsigned num);

/**
 * \brief Create a deep copy of the supplied query
 * \param query The query_data structure to copy
 * \return A deep copy of \a query
 */
struct query_data*
query_clone(const struct query_data* query);

/**
 * \brief Deallocate and clear internal data from a query_data structure
 * This function should be called before free() on a query_data structure
 * Note that the query_data structure itself is owned by the user and is not
 * deallocated. Only its internal fields are deallocated
 * \param query The query_data structure to internally deallocate
 */
void
query_destroy(struct query_data* query);

#endif /* __QUERY_PARSER_H__ */
