/* This file is part of Super. A 3D pattern-matching program.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAST_BOUNDS_CHECK_H__
#define FAST_BOUNDS_CHECK_H__

/**
 * \file fast_bounds_check.h
 * \brief Implements a fast "short-cut" bounds check on the RMSD between
 * reference and query structural fragments
 */

#include <vector_set.h>
#include <query_parser.h>

/**
 * \brief A bounds check
 * See papers:
 * - M. Rustici, A. Lesk (1994). Three-Dimensional Searching for Recurrent
 *    Structural Motifs in Data Bases of Protein Structures.
 *    Journal Of Computational Biology 1(2): 121-132
 *
 * \param ref The reference structural fragment
 * \param query The query structural fragment
 * \param threshold The RMSD threshold
 * \return True if the computed lowerbound is less-than the threshold,
 *         otherwise return false
 */
float
default_lower_bound(const struct db_vector *restrict ref,
                    const float *restrict query,
		    const uint32_t size);

#endif /* FAST_BOUNDS_CHECK_H__ */
