/* This file is part of Super. A 3D pattern-matching program.
 * Utility functions
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file util.h
 * \brief Useful utility routines.
 */


#ifndef UTIL_H__
#define UTIL_H__

/**
 * \brief Search the $SUPER_DB_PATH for a file called "query.pdb"
 * \param path A list of paths to be populated (this function will only fill 1)
 * \return EEXIST If no query.pdb file is found in $SUPER_DB_PATH
 * \return EMEM On allocation error
 * \return ENONE On success
 */
int
get_query_path_auto(char **path);

#endif /* __UTIL_H__ */
