/* This file is part of Super. A 3D pattern-matching program.
 * Interface to set up and run the main search loop
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PIPELINE_H__
#define PIPELINE_H__

/**
 * \file super_runner.h
 * \brief Handles the main search loop for Super
 */

#include <arguments.h>
#include <result.h>
#include <vector_set.h>

#include <pthread.h>

/**
 * \struct pipeline
 * \brief Stores data necessary for the main search loop
 */
struct pipeline{
  float threshold;            // RMSD threashold
  struct result* results;     // Storage for match results
  struct result* last_result; // Most recent match result
  uint32_t num_results;       // Number of match results

  struct query_data* query;   // Query coordinate data
  struct vector_set* ref;     // Moving window of reference coordinates
};

/**
 * \struct super_runner
 * \brief Main handler data for setup of the main search loop(s)
 */
struct super_runner{
  uint32_t n_pipelines;       // Number of threads (main search loops) to run
  struct pipeline* pipelines; // Search loop local data
  struct db* db;              // Database to search over
  struct query_data* query;   // Query coordinate data to match
  struct vector_set* ref;     // Moving window of reference coordinate data
  struct result* results;     // Storage for aggregrated results
};

/**
 * \brief Setup data structures for each thread (pipeline)
 * \param super An allocated handler structure to fill
 * \param args User supplied arguments used to fille the handler structure
 * \return E* on error (calls setup functions for: database, query, vector_set)
 * \return ENONE otherwise
 */
int
super_runner_setup(struct super_runner* super,
                   const struct arguments* args);

/**
 * \brief Run the main search loop(s)
 *
 * Call super_runner_setup() on the handler structure before calling this
 * function with the same handler function
 * \param super An allocated and set-up super runner
 * \return ENONE
 */
int
super_runner_run(struct super_runner* super);

/**
 * \brief Internal deallocation for the handler structure
 *
 * Call free() on the handler structure after using this destroy function
 * because this does not deallocate the structure itself, only its internal fields
 * \param super A handler structure
 */
void
super_runner_destroy(struct super_runner* super);

#endif /* PIPELINE_H__ */
