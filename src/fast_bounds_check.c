/* This file is part of Super. A 3D pattern-matching program.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdio.h>
#include <util.h>

#include <fast_bounds_check.h>
#include <numeric.h>

/**
 * \file fast_bounds_check.c
 * \brief Implements a fast "short-cut" bounds check on the RMSD between
 * reference and query structural fragments
 */

/* Implements the least squares lower-bound
 * E^2 >= \sum_i (|u_i| - |v_i|)^2
 */
__attribute__((target_clones("avx2","sse4.2","default")))
float
default_lower_bound(const struct db_vector *restrict ref,
                    const float *restrict query,
		    const uint32_t size)
{
  float rms = 0.0f, dist = 0.0f;
  
  //query->size is always the correct number of coordinate vectors
  //because it doesn't include any gaps
  for(unsigned i = 0; i < size; i++){
    dist = *query - vector_norm(ref);
    rms += dist*dist;
    query++; ref++;
  }
  return sqrtf(rms/(float)size);
}
