/* This file is part of Super. A 3D pattern-matching program.
 * Parses a subset of Brookhaven PDB file data to create a query
 * vector set
 * According to format version 3.30
 * See: http://www.wwpdb.org/documentation/file-format
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <query_parser.h>
#include <numeric.h>

/**
 * \file query_parser.c
 * \brief Parsing a subset of the Brookhaven PDB format version 3.30
 */

/* Each row of a Brookhaven PDB format file is 81 columns
 * This buffer size is enough to store 125 rows at a time */
#define MAX_BUFFER_SIZE 10125

/**
 * \brief translate a three-letter-code into a single-letter-code
 * \param tlc String representation of the three-lettter-code
 * \return A single-letter-code if the three-letter-code matches, otherwise 'X'
 */
char
single_letter_code(const char* tlc)
{
  if(strncmp("ALA", tlc, 3) == 0){
    return 'A';
  }
  if(strncmp("ARG", tlc, 3) == 0){
    return 'R';
  }
  if(strncmp("ASN", tlc, 3) == 0){
    return 'N';
  }
  if(strncmp("ASP", tlc, 3) == 0){
    return 'D';
  }
  if(strncmp("CYS", tlc, 3) == 0){
    return 'C';
  }
  if(strncmp("GLU", tlc, 3) == 0){
    return 'E';
  }
  if(strncmp("GLN", tlc, 3) == 0){
    return 'Q';
  }
  if(strncmp("GLY", tlc, 3) == 0){
    return 'G';
  }
  if(strncmp("HIS", tlc, 3) == 0){
    return 'H';
  }
  if(strncmp("ILE", tlc, 3) == 0){
    return 'I';
  }
  if(strncmp("LEU", tlc, 3) == 0){
    return 'L';
  }
  if(strncmp("LYS", tlc, 3) == 0){
    return 'K';
  }
  if(strncmp("MET", tlc, 3) == 0){
    return 'M';
  }
  if(strncmp("PHE", tlc, 3) == 0){
    return 'F';
  }
  if(strncmp("PRO", tlc, 3) == 0){
    return 'P';
  }
  if(strncmp("SER", tlc, 3) == 0){
    return 'S';
  }
  if(strncmp("THR", tlc, 3) == 0){
    return 'T';
  }
  if(strncmp("TRP", tlc, 3) == 0){
    return 'W';
  }
  if(strncmp("TYR", tlc, 3) == 0){
    return 'Y';
  }
  if(strncmp("VAL", tlc, 3) == 0){
    return 'V';
  }
  if(strncmp("SEC", tlc, 3) == 0){
    return 'U';
  }
  if(strncmp("PYL", tlc, 3) == 0){
    return 'O';
  }
  if(strncmp("ASX", tlc, 3) == 0){
    return 'B';
  }
  if(strncmp("GLX", tlc, 3) == 0){
    return 'Z';
  }
  if(strncmp("XLE", tlc, 3) == 0){
    return 'J';
  }
  return 'X';
}

/**
 * \brief Create and perform internal allocation for a query_data structure
 * \param query The query_data to initialise
 * \param atom An array of 3D coordinates
 * \param sequence A sequence of single-letter-codes
 * \param atom_idx The length/number of coordinates and single-letter-codes
 * \return EMEM on failed allocation
 * \return ENONE otherwise
 */
int
generate_query_data(struct query_data* query, struct db_vector* atom,
                    const char* sequence, size_t atom_idx)
{
  query->centered = calloc(atom_idx, sizeof(struct db_vector));
  if(!query->centered){
    return EMEM;
  }
  db_vector_center_initial(query->centered, atom, atom_idx);

  query->norm = malloc(atom_idx * sizeof(float));
  if(!query->norm){
    return EMEM;
  }
  compute_vector_norms(query->centered, query->norm, atom_idx);
  
  query->sequence = calloc(atom_idx + 1, 1);
  if(!query->sequence){
    free(query->centered);
    return EMEM;
  }
  strncpy(query->sequence, sequence, atom_idx);
  
  query->size = atom_idx;
  return ENONE;
}

int
query_parse(struct query_data *query, const char** filename, unsigned num)
{
  if(!query || !filename){
    return EMEM;
  }
  //check that the filenames are valid
  if(num == 0) return EINVALID;
  int err = ENONE;
  struct stat file;
  unsigned estimate_num_atoms = 0;
  struct db_vector* atom = NULL;
  char* sequence = NULL;
  char *buffer = NULL;
  ssize_t buffer_size = 0;
  ssize_t count = 0;
  size_t atom_idx = 0;
  unsigned real_num_files = 0;
  const char** checked_filename = malloc(num * sizeof(const char*));
  
  for(unsigned i = 0; i < num; i++){
    err = stat(filename[i], &file);
    if(err){
      fprintf(stderr, "!!WARNING!!: \"%s\" does not exist. Ignoring...\n",
              filename[i]);
      err = EEXIST;
      continue;
    }else if(!S_ISREG(file.st_mode)){
      fprintf(stderr, "!!ERROR!!: \"%s\" is not a regular file.\n",
              filename[i]);
      err = EINVALID;
      continue;
    }

    //a PDB line is 81 (including the newline) columns wide
    estimate_num_atoms += (file.st_size / 81) + 1;
    if(estimate_num_atoms > 1000){
      err = EOOB;
      goto cleanup;
    }

    checked_filename[real_num_files] = filename[i];
    real_num_files++;

    //never allocate more than MAX_BUFFER_SIZE bytes
    if(file.st_size > buffer_size){
      buffer_size = (file.st_size > MAX_BUFFER_SIZE
                     ? MAX_BUFFER_SIZE : file.st_size);
    }
  }

  if(real_num_files == 0){
    goto cleanup;
  }else{
    err = ENONE;
  }

  //allocate memory for the file data buffer
  buffer = malloc(buffer_size + 1);
  //allocate some memory for a plausible number of db_vector structures
  atom = calloc(estimate_num_atoms, sizeof(struct db_vector));
  if(!atom){
    err = EMEM;
    atom = NULL;
    goto cleanup;
  }
  sequence = calloc(estimate_num_atoms + 1, 1);
  if(!sequence){
    err = EMEM;
    sequence = NULL;
    goto cleanup;
  }

  query->divider = 0;

  for(unsigned i = 0; i < real_num_files; i++){
    //we only care about the ATOM record
    int fd = open(checked_filename[i], O_RDONLY);
    if(fd == -1){
      err = EUNKNOWN;
      goto cleanup;
    }
    if(i == 1){
      query->divider = atom_idx;
    }

    while((count = read(fd, buffer, buffer_size)) > 0){
      int line_begin_offset = 0;
      while(line_begin_offset < count){
        if(strncmp("ATOM ", &buffer[line_begin_offset], 5) != 0){
          line_begin_offset+=81;
	  continue;
        }
        if(strncmp("CA", &(buffer[line_begin_offset+13]), 2) != 0){
          line_begin_offset+=81;
	  continue;
        }

        sequence[atom_idx] = single_letter_code(&buffer[line_begin_offset+17]);
        
        atom[atom_idx].X = strtod(&buffer[line_begin_offset+31], NULL);
        atom[atom_idx].Y = strtod(&buffer[line_begin_offset+39], NULL);
        atom[atom_idx].Z = strtod(&buffer[line_begin_offset+47], NULL);

        atom_idx++;
        line_begin_offset += 81;
      }
    }
    close(fd);
  }

  if(atom_idx > 1){
    err = generate_query_data(query, atom, sequence, atom_idx);
  }else{
    err = EINVALID;
  }

 cleanup:
  if(checked_filename) free(checked_filename);
  if(sequence) free(sequence);
  if(atom) free(atom);
  if(buffer) free(buffer);
  return err;
}

struct query_data*
query_clone(const struct query_data* query)
{
  struct query_data* clone = malloc(sizeof(struct query_data));
  clone->norm = malloc(query->size * sizeof(float));
  memcpy(clone->norm, query->norm, query->size * sizeof(float));
  clone->centered = malloc(query->size * sizeof(struct db_vector));
  memcpy(clone->centered, query->centered,
         query->size * sizeof(struct db_vector));
  clone->sequence = strdup(query->sequence);
  clone->size = query->size;
  clone->divider = query->divider;
  
  return clone;
}

void
query_destroy(struct query_data* query)
{
  if(!query) return;
  if(query->norm){
    free(query->norm);
    query->norm = NULL;
  }
  if(query->centered){
    free(query->centered);
    query->centered = NULL;
  }
  if(query->sequence){
    free(query->sequence);
    query->sequence = NULL;
  }
  query->size = 0;
  query->divider = 0;
  return;
}
