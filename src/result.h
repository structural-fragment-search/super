/* This file is part of Super. A 3D pattern-matching program.
 * Store and print out match results.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __RESULT_H__
#define __RESULT_H__

#include <stdint.h>

#include <database.h>
#include <query_parser.h>
#include <arguments.h>

/**
 * \file result.h
 * \brief Storage and output of query match results
 */

/**
 * \struct result
 * \brief Linked list of necessary result data
 */
struct result {
  struct result* next;  // Pointer to the next item in the linked list
  char ID[4];           // Four character PDB identifier (see \a database.h)
  uint32_t meta_offset; // Offset index into the database metadata
  uint32_t size;        // Size of the match
  float rmsd;           // RMSD of the match
  char chain;           // Chain identifer for the match
};

/**
 * \brief Append a result to the list
 * Walk along list until the end, then store new_result and the end
 * \param list A pointer to a list
 * \param new_result A new result to store at the end of the list
 * 
 * Worst case complexity: O(N), N is the size of the list
 * This can be made constand by storing a pointer to the end of the list
 * See: super_runner.c:run_pipe()
 */
void
result_list_append(struct result* list, struct result* new_result);

/**
 * \brief Print out detailed results from the list
 * \param list The list to print from
 * \param db The database that corresponds to the result list
 * \param query The query coordinate- and meta-data corresponding to the match
 * \param args User supplied arguments (e.g. where and what to print)
 */
int
result_list_print(struct result *restrict list,
                  const struct db *restrict db,
                  const struct query_data *restrict query,
                  const struct arguments *restrict args);

/**
 * \brief Deallocate the list (free all memory)
 *
 * Do not call free() on the list after calling this function. Recursively
 * walks the list to correctly deallocate all results
 * \param list A pointer to the list to deallocate
 */
void
result_list_destroy(struct result** list);

#endif /* __RESULT_H__ */
