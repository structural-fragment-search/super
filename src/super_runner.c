/* This file is part of Super. A 3D pattern-matching program.
 * Interface to set up and run the main search loop
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file super_runner.c
 * \brief Handles the main search loop for Super
 */

#define _XOPEN_SOURCE 500

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <assert.h>

#include <super_runner.h>
#include <serrors.h>
#include <matrix.h>
#include <util.h>
#include <fast_bounds_check.h>
#include <database.h>
#include <query_parser.h>

/**
 * \brief The main super search loop
 *
 * This function is called when creating a search worker thread
 */
static void*
run_pipe(void* p);

/**
 * \brief Create and append a match result to the list for a pipeline
 */
static void
append_match(struct pipeline* p, const float rmsd);

void
super_runner_destroy(struct super_runner* super)
{
  if(!super) return;

  if(super->pipelines){
    if(super->n_pipelines > 1){
      for(uint32_t i = 0; i < super->n_pipelines; i++){
        if(super->pipelines[i].results){
          free(super->pipelines[i].results);
        }
        super->pipelines[i].last_result = NULL;
        if(super->pipelines[i].query){
          query_destroy(super->pipelines[i].query);
          free(super->pipelines[i].query);
        }
        if(super->pipelines[i].ref){
          vector_set_destroy(super->pipelines[i].ref);
          free(super->pipelines[i].ref);
        }
      }
    }
    free(super->pipelines);
    super->pipelines = NULL;
  }

  if(super->db){
    db_destroy(super->db);
    free(super->db);
    super->db = NULL;
  }

  if(super->query){
    query_destroy(super->query);
    free(super->query);
    super->query = NULL;
  }

  if(super->ref){
    vector_set_destroy(super->ref);
    free(super->ref);
    super->ref = NULL;
  }

  if(super->results){
    result_list_destroy(&super->results);
    super->results = 0;
  }

  super->n_pipelines = 0;
}

static int
initialise_database(struct super_runner* super, const char* filename)
{
  super->db = malloc(sizeof(struct db));
  if(!super->db){
    perror("super_runner_setup: database");
    return EMEM;
  }
  return db_load_from_file(filename, super->db);
}

static int
initialise_query(struct super_runner* super,
                 const char** query_files, unsigned num)
{
  super->query = calloc(1, sizeof(struct query_data));
  if(!super->query){
    perror("super_runner_setup: query");
    return EMEM;
  }

  return query_parse(super->query, query_files, num);
}

static int
initialise_reference(struct super_runner* super,
                     const struct arguments* args)
{
  super->ref = malloc(sizeof(struct vector_set));
  if(!super->ref){
    perror("super_runner_setup: reference");
    return EMEM;
  }

  return vector_set_create(super->ref, super->db, super->query->size,
                           args->coil, super->query->divider);
}

static int
initialise_segmentation(struct super_runner* super,
                        const struct arguments *restrict args)
{
  int err = ENONE;
  uint32_t segments = args->threads, total = 0, remainder = 0;
  const uint32_t entries = super->db->entry_count;
  if(args->threads > entries){
    segments = 1;
  }

  //number of entries given to each segment
  uint32_t entry_segmentation[segments];
  memset(entry_segmentation, 1, segments);

  super->n_pipelines = segments;
  super->pipelines = calloc(segments, sizeof(struct pipeline));
  if(entries > segments){
    for(uint32_t i = 0; i < segments; i++){
      entry_segmentation[i] = entries / segments;
      total += entry_segmentation[i];
    }
  }
  remainder = entries - total; //integer division rounds down
  // Divide the remainder
  while(remainder){
    entry_segmentation[remainder--]++;
  }

  //initialise cloned vector sets
  if(segments == 1){
    super->pipelines[0].threshold = args->threshold;
    super->pipelines[0].query = super->query;
    super->pipelines[0].ref = super->ref;
    return err;
  }

  total = 0;
  for(uint32_t i = 0; i < segments; i++){
    struct vector_set* restricted_ref = vector_set_clone(super->ref);
    if(!restricted_ref){
      fprintf(stderr, "vector_set_clone() failed!\n");
      return EMEM;
    }
    
    err = vector_set_restrict(restricted_ref, total,
                              total + entry_segmentation[i] - 1);
    super->pipelines[i].threshold = args->threshold;
    super->pipelines[i].query = query_clone(super->query);
    super->pipelines[i].ref = restricted_ref;
    if(err) break;
    total += entry_segmentation[i];
  }
  return err;
}

int
super_runner_setup(struct super_runner* super,
                   const struct arguments* args)
{
  int err = initialise_database(super, args->search_database);
  if(err){
    return err;
  }
  
  err = initialise_query(super, (const char**)args->query, args->n_query);
  if(err){
    return err;
  }

  err = initialise_reference(super, args);
  if(err){
    return err;
  }

  err = initialise_segmentation(super, args);
  if(err){
    return err;
  }

  return ENONE;
}

int
super_runner_run(struct super_runner* super)
{
  pthread_attr_t thread_attr;
  pthread_attr_init(&thread_attr);
  pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_JOINABLE);
  pthread_t* threads = malloc(super->n_pipelines * sizeof(pthread_t));
  
  for(uint32_t i = 0; i < super->n_pipelines; i++){
    assert(super->pipelines[i].query != NULL);
    assert(super->pipelines[i].ref != NULL);

    pthread_create(&threads[i], &thread_attr, run_pipe,
		   (void*)&(super->pipelines[i]));
  }
  pthread_attr_destroy(&thread_attr);
  
  struct result* intermediate = super->results;
  for(uint32_t i = 0; i < super->n_pipelines; i++){
    pthread_join(threads[i], NULL);

    //gather (move) results
    if(super->pipelines[i].num_results > 0){
      if(super->results == NULL){
        super->results = super->pipelines[i].results;
      }else{
        result_list_append(intermediate, super->pipelines[i].results);
      }
      intermediate = super->pipelines[i].last_result;
      super->pipelines[i].results = super->pipelines[i].last_result = NULL;
    }
  }

  free(threads);
  return ENONE;
}

void
append_match(struct pipeline* p, const float rmsd)
{
  struct result* match = calloc(1, sizeof(struct result));
  match->next = NULL;
  memcpy(&match->ID, &p->ref->ID[0], 4);
  match->meta_offset = p->ref->base_meta_index + p->ref->current_vector;
  match->size = p->query->size + p->ref->coil_length;
  match->rmsd = rmsd;
  match->chain = p->ref->chain;
          
  if(p->results == NULL){
    p->results = match;
    p->last_result = match;
    p->num_results = 1;
  }else{
    result_list_append(p->last_result, match);
    p->last_result = match;
    p->num_results++;
  }
}

/* This is the main program loop
 * FRAG -> LB(FRAG) -> RMSD(FRAG) -> FRAG++
 * It is the responsibility of all the setup code to ensure
 * pre-conditions are met. This loop performs NO ERROR CHECKING to
 * run as fast as possible
 */
static void*
run_pipe(void* p)
{
  int err = ENONE;
  struct pipeline* pipeline = (struct pipeline*)p;
  const float gg_threshold = (pipeline->threshold * pipeline->threshold)
    * pipeline->query->size;
  float rmsd = 0.0f;

  while(err == ENONE){
    if(pipeline->threshold > default_lower_bound(pipeline->ref->centered,
						 pipeline->query->norm,
						 pipeline->query->size)){
      if(!superpose(pipeline->query, pipeline->ref,
		    &rmsd, gg_threshold)){
        if(rmsd <= pipeline->threshold){
	  append_match(pipeline, rmsd);
        }
      }
    }

    err = vector_set_proceed(pipeline->ref);
  }
  
  return NULL;
}
