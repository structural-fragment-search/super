/* This file is part of Super. A 3D pattern-matching program.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If the -q, --query option is not passed, will look for a query.pdb
 * in the path(s) specified in SUPER_DB_PATH environment variable.
 * Search paths are colon separated.
 * Example command line:
 * super -q query.pdb -t 1.5 -o output.txt database.sdb
 * Database is always required as the primary input parameter
 * to the program, all other parameters are optional and the program
 * will run with the default values.
 */

/**
 * \file main.c
 * \brief Main entry point for the super program
 * This file contains scaffolding code to handle user input,
 * setup the environment to run the database search.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

#ifdef HAVE_CONFIG_H
#include <super_config.h>
#endif // HAVE_CONFIG_H

#include <super_runner.h>
#include <util.h>
#include <arguments.h>
#include <serrors.h>

/* Search RMSD threshold when user does not supply one */
#define DEFAULT_THRESHOLD (2.0)

/**
 * \brief Initialise and check input data and arguments for contradictions
 * and user errors.
 * If the user did not supply a query file, search the $SUPER_DB_PATH
 * environment variable for a query.pdb file
 */
static int
initialise(struct arguments* arguments)
{
  if((arguments->coil > 0) && (arguments->n_query <= 1)){
    fprintf(stderr, "Ungapped query used!\n");
    return EINVALID;
  }

  const unsigned long cores = sysconf(_SC_NPROCESSORS_ONLN);
  if(arguments->threads < 1){
    arguments->threads = (unsigned)cores;
  }else if(arguments->threads > (cores*2)){
    arguments->threads = (unsigned)cores*2;
  }

  if(arguments->n_query == 0){ //Search the $SUPER_DB_PATH environment variable
    int err = get_query_path_auto(arguments->query);
    if(err){
      print_err_message(err);
      fprintf(stderr,
              "SUPER_DB_PATH does not contain a valid \"query.pdb\" file.\n");
      exit(1);
    }
    arguments->n_query = 1;
  }

  return ENONE;
}

/**
 * \brief Print the paper citation for Super to stderr
 */
static void
print_citation(void)
{
  fprintf(stderr,
          "+==================================================================="
          "====================================+\n"
          "| Super Citation:                                                   "
          "                                    |\n"
          "| Collier, J. H., Lesk, A. M., de la Banda, M. G., & Konagurthu, A. "
          "S. (2012).                          |\n"
          "| Super: a web server to rapidly screen superposable oligopeptide fr"
          "agments from the protein data bank. |\n"
          "| Nucleic Acids Research, 40(W1), W334-W339.                        "
          "                                    |\n"
          "+==================================================================="
          "====================================+\n");
}

/**
 * Super runs in five phases
 * I   - Argument parsing and internal initialisation
 * II  - Setup of the database and query
 * III - Running the search
 * IV  - Printing the results
 * V   - Final cleanup and exit
 * Each step proceeds iff each previous step did not encounter an error
 */
int
main(int argc, char** argv)
{
  int err;
  struct super_runner super;
  struct arguments arguments;
  memset(&super, 0, sizeof(struct super_runner));
  memset(&arguments, 0, sizeof(struct arguments));
  arguments.verbose = 1;
  arguments.threshold = DEFAULT_THRESHOLD;
  parse_arguments(argc, argv, &arguments);
  err = initialise(&arguments);

  if(err == ENONE){
    err = super_runner_setup(&super, &arguments);
  }

  if(err == ENONE){ //everything is ready, lets run the search
    err = super_runner_run(&super);
  }

  if(err == ENONE){ //search complete, print the results
    print_citation();
    if(super.results != NULL){
      err = result_list_print(super.results, super.db,
			      super.query, &arguments);
    }else{
      fprintf(arguments.output, "NO RESULTS\n");
    }
    print_citation();
  }

  handle_error(err);
  super_runner_destroy(&super);
  free_arguments_internal(&arguments);
  pthread_exit(&err);
  return err;
}
