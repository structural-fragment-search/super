/* This file is part of Super. A 3D pattern-matching program.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _DEFAULT_SOURCE

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <database.h>
#include <serrors.h>

#include <assert.h>

int
db_load_from_file(const char* db, struct db* database)
{
  int err, fd;
  struct stat d_stat;
  if(!db) return EMEM;
  if(!database) return EMEM;
  memset(database, 0, sizeof(struct db));

  //check that the file exsists and is at least a minimum size
  err = stat(db, &d_stat);
  if(err < 0){
    return EEXIST;
  }else if(!S_ISREG(d_stat.st_mode)){
    return EINVALID;
  }
  database->buffer_size = (size_t)d_stat.st_size;
  if(database->buffer_size < (sizeof(struct db_header)
                              + sizeof(struct db_entry_header)
                              + 3 * sizeof(struct db_vector))){
    return EINVALID;
  }
  fd = open(db, O_RDONLY);
  if(fd == -1){
    return EINVALID;
  }

  database->buffer = mmap(NULL, database->buffer_size, PROT_READ,
                          MAP_PRIVATE | MAP_NORESERVE, fd, 0);
  if(database->buffer == MAP_FAILED){
    db_destroy(database);
    return EMEM;
  }
  err = madvise(database->buffer, database->buffer_size,
                MADV_SEQUENTIAL | MADV_WILLNEED);
  if(err){
    perror("madvise");
  }

  struct db_header header;
  memcpy(&header, database->buffer, sizeof(struct db_header));
  uint32_t metadata_offset = header.metadata;
  database->metadata = (struct db_meta const*)
    ((char*)database->buffer + metadata_offset);
  database->entry_count = header.entry_count;

  err = close(fd);
  if(err){
    return EMEM;
  }
  
  if(header.magic != MAGIC){
    db_destroy(database);
    return EMAGIC;
  }

  if(header.entry_count < 1){
    db_destroy(database);
    return EINVALID;
  }

  return ENONE;
}

void
db_destroy(struct db* database)
{
  int err;
  if(!database) return;

  err = munmap(database->buffer, database->buffer_size);
  if(err){
    perror("munmap");
  }

  database->buffer = NULL;
  database->metadata = NULL;
  database->buffer_size = 0;
  database->entry_count = 0;
}

const struct db_meta*
db_meta_by_index(const struct db* restrict database, uint32_t index)
{
  return &database->metadata[index];
}

__attribute__((target_clones("avx2","sse4.2","default")))
void
db_vector_center_initial(struct db_vector *restrict dest,
                         const struct db_vector *restrict src,
                         const uint32_t number)
{
  float Xav = 0.0;
  float Yav = 0.0;
  float Zav = 0.0;

  for(size_t i = 0; i < number; i++){
    Xav += src[i].X;
    Yav += src[i].Y;
    Zav += src[i].Z;
  }

  Xav /= (float)number;
  Yav /= (float)number;
  Zav /= (float)number;

  for(size_t i = 0; i < number; i++){
    dest[i].X = src[i].X - Xav;
    dest[i].Y = src[i].Y - Yav;
    dest[i].Z = src[i].Z - Zav;
  }
}
