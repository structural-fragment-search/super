/* This file is part of Super. A 3D pattern-matching program.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include <vector_set.h>
#include <matrix.h>
#include <serrors.h>
#include <util.h>
#include <serrors.h>


#ifdef HAVE_CONFIG_H
#include <super_config.h>
#endif //HAVE_CONFIG_H

/**
 * \file matrix.c
 * \brief Implements Kearsleys method to compute the RMSD between structural
 * fragments.
 * The functions in this file perform minimal error checking. Pay careful
 * attention to preconditions before using them.
 */

/**
 * \brief Compute an index (row, column) into a 4x4 matrix
 *
 * The matrix, M, is real square symmetric matrix
 * until we perform jacobi rotations, then it
 * gradually becomes triangular.
 * therefore we really need to store all 16
 * values:
 * M = +--+--+--+--+
 *     |00|01|02|03|
 *     +--+--+--+--+
 *     |04|05|06|07|
 *     +--+--+--+--+
 *     |08|09|10|11|
 *     +--+--+--+--+
 *     |12|13|14|15|
 *     +--+--+--+--+
 */
#define IDX(r, c) (4*r + c)

/**
 * \struct matrix_data
 * \brief Stores required information for a 4x4 matrix.
 *
 * The matrix data is stored in \a .matrix, this data is manipulated to find
 * \a eigenvalues and \a eigenvectors.
 */
struct matrix_data{
  float data[16];
};

/* Indexes used when pre-computing a 4x4 matrix for Kearsley's algorithm */
#define XXM 0
#define XXP 1
#define YYM 2
#define YYP 3
#define ZZM 4
#define ZZP 5

/* A value regarded as "close enough" to 0.0 */
#define MIN_VALUE 0.1f
#define MAX(a,b) (a > b ? a : b)

#define PRINT_MATRIX(m) {						\
    printf("[%.3f %.3f %.3f %.3f]\n"					\
	   "[%.3f %.3f %.3f %.3f]\n"					\
	   "[%.3f %.3f %.3f %.3f]\n"					\
	   "[%.3f %.3f %.3f %.3f]\n",                                   \
	   m.data[IDX(0,0)],                                            \
	   m.data[IDX(0,1)],                                            \
	   m.data[IDX(0,2)],                                            \
	   m.data[IDX(0,3)],                                            \
	   m.data[IDX(1,0)],                                            \
	   m.data[IDX(1,1)],                                            \
	   m.data[IDX(1,2)],                                            \
	   m.data[IDX(1,3)],                                            \
	   m.data[IDX(2,0)],                                            \
	   m.data[IDX(2,1)],                                            \
	   m.data[IDX(2,2)],                                            \
	   m.data[IDX(2,3)],                                            \
	   m.data[IDX(3,0)],                                            \
	   m.data[IDX(3,1)],                                            \
	   m.data[IDX(3,2)],                                            \
	   m.data[IDX(3,3)]);                                           \
  }


/**
 * \struct matrix_max
 * \brief Stores value and indexe to the largest value in the 4x4 Kearsley matrix
 * This is used by the Jacobi diabonalisation method
 */
struct matrix_max{
  int col;
  int row;
  float val;
};


/**
 * \brief Compute the 4x4 Kearsley matrix based on input structural fragments
 * \param qry The query coordinate vector
 * \param ref The reference coordinate vector
 * \param M An array with length=16 representing a 4x4 matrix
 *
 * This function fills M with a real symmetrix matrix using Kearsley's method
 */
static void
precalculate_matrix(const struct query_data *restrict qry,
                    const struct vector_set *restrict ref,
                    float *restrict M);

/**
 * \brief Compute the Maxiumum Upper Off-Diagonal Element of a 4x4 matrix
 * Store the value and indexes to that element in a matrix_max structure
 * \param M The matrix from which to compute the maximum upper off-diagonal
 *          element
 * \param max Where the function stores the max value and it's (row,col) index
 * \return The maximum upper off-diagonal value
 */
static float
max_elem(const float *restrict M,
         struct matrix_max * restrict max) __attribute__((hot));

/**
 * \brief Compute a Jacobi rotation on a 4x4 matrix
 * \param M The 4x4 matrix to rotate (a length 16 float array representation)
 * \param max A structure containing the maximum upper off-diagonal value
 *            and index (row, col) of that value
 * This function rotates M in place
 */
static void
rotate_matrix(float *restrict M, const struct matrix_max *restrict max);

/**
 * \brief Compute a Gershgorin circles check against an eigenvalue threshold
 * \param M The 4x4 matrix to check (a length 16 float array representation)
 * \param threshold The eigenvalue threshold
 * \return 0 if within the threshold, otherwise 1
 */
static int
gershgorin(const float *restrict M, const float threshold);

__attribute__((target_clones("avx2","sse4.2","default")))
int
superpose(const struct query_data *restrict qry,
          const struct vector_set *restrict ref,
          float *rmsd, const float threshold)
{
  struct matrix_max mmax;
  struct matrix_data data;
  float min_eval = FLT_MAX, val;

  memset(&data, 0, sizeof(struct matrix_data));
  precalculate_matrix(qry, ref, data.data);

  for(unsigned i = 0; i < 20; i++){
    if(gershgorin(data.data, threshold)){
      return EINVALID;
    }
    max_elem(data.data, &mmax);
    if(mmax.val <= MIN_VALUE){ // Finished diag: Extract eigenvalues
      break;
    }
    rotate_matrix(data.data, &mmax);
  }

  for(unsigned k = 0; k < 4; k++){
    val = data.data[IDX(k,k)];
    if(val < min_eval) min_eval = MAX(val, .0f);
  }
  *rmsd = sqrtf(min_eval / qry->size);
  return ENONE;
}


__attribute__((target_clones("avx2","sse4.2","default")))
static void
precalculate_matrix(const struct query_data *restrict qry,
                    const struct vector_set *restrict ref,
                    float *restrict M)
{
  // Calculate Right diagonal, then copy off-diagonal elements as this must be a symmetric matrix.
  unsigned long i, j = 0;
  struct db_vector *v_u;
  struct db_vector *v_v;

  float precalc[] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
  for(i = 0; i < qry->size; i++, j++){
    v_u = &qry->centered[i];
    v_v = &ref->centered[j];

    precalc[XXM] = v_v->X - v_u->X;
    precalc[XXP] = v_v->X + v_u->X;
    precalc[YYM] = v_v->Y - v_u->Y;
    precalc[YYP] = v_v->Y + v_u->Y;
    precalc[ZZM] = v_v->Z - v_u->Z;
    precalc[ZZP] = v_v->Z + v_u->Z;

    M[IDX(0,0)] += ((precalc[XXM] * precalc[XXM]) + (precalc[YYM] * precalc[YYM]) + (precalc[ZZM] * precalc[ZZM]));
    M[IDX(1,1)] += ((precalc[XXM] * precalc[XXM]) + (precalc[YYP] * precalc[YYP]) + (precalc[ZZP] * precalc[ZZP]));
    M[IDX(2,2)] += ((precalc[XXP] * precalc[XXP]) + (precalc[YYM] * precalc[YYM]) + (precalc[ZZP] * precalc[ZZP]));
    M[IDX(3,3)] += ((precalc[XXP] * precalc[XXP]) + (precalc[YYP] * precalc[YYP]) + (precalc[ZZM] * precalc[ZZM]));
	
    M[IDX(0,1)] += ((precalc[YYP] * precalc[ZZM]) - (precalc[YYM] * precalc[ZZP]));
    M[IDX(0,2)] += ((precalc[XXM] * precalc[ZZP]) - (precalc[XXP] * precalc[ZZM]));
    M[IDX(0,3)] += ((precalc[XXP] * precalc[YYM]) - (precalc[XXM] * precalc[YYP]));
	
    M[IDX(1,2)] += ((precalc[XXM] * precalc[YYM]) - (precalc[XXP] * precalc[YYP]));
    M[IDX(1,3)] += ((precalc[XXM] * precalc[ZZM]) - (precalc[XXP] * precalc[ZZP]));
      
    M[IDX(2,3)] += ((precalc[YYM] * precalc[ZZM]) - (precalc[YYP] * precalc[ZZP]));
  }

  M[IDX(1,0)] = M[IDX(0,1)];
  M[IDX(2,0)] = M[IDX(0,2)];
  M[IDX(3,0)] = M[IDX(0,3)];

  M[IDX(2,1)] = M[IDX(1,2)];
  M[IDX(3,1)] = M[IDX(1,3)];

  M[IDX(3,2)] = M[IDX(2,3)];
}

/** 
 * Returns a pointer to the max off-diagonal element of the upper
 * triangular matrix
 */
__attribute__((target_clones("avx2","sse4.2","default")))
static float
max_elem(const float *restrict M, struct matrix_max *restrict max)
{
  float val = 0;
  max->val = -1.0;
  for(unsigned i = 0; i < 3; i++){
    for(unsigned j = i+1; j < 4; j++){
      val = fabsf(M[IDX(i,j)]);
      if(val > max->val){
        max->row = i;
	max->col = j;
	max->val = val;
      }
    }
  }
  return max->val;
}

__attribute__((target_clones("avx2","sse4.2","default")))
static void
rotate_matrix(float *restrict M, const struct matrix_max *restrict max)
{
  float diff = M[IDX(max->col,max->col)] - M[IDX(max->row,max->row)];
  float phi, t, c, s, tau, temp;
  int i;

  if(fabsf(M[IDX(max->row,max->col)]) <= MIN_VALUE){
    t = M[IDX(max->row,max->col)] / diff;
  }else{
    phi = diff / (2.0 * M[IDX(max->row,max->col)]);
    t = 1.0f / (fabsf(phi) + sqrtf((phi*phi) + 1.0f));
    if(phi < 0){ t = -t; }
  }

  c = 1.0f / sqrtf(t*t + 1.0f);
  s = t*c;
  tau = s/(1.0f + c);
  temp = M[IDX(max->row,max->col)];
  M[IDX(max->row,max->col)] = 0;
  M[IDX(max->row,max->row)] = M[IDX(max->row,max->row)] - (t*temp);
  M[IDX(max->col,max->col)] = M[IDX(max->col,max->col)] + (t*temp);
  for(i = 0; i < max->row; i++){ // Case i < max->row
    temp = M[IDX(i,max->row)];
    M[IDX(i,max->row)] = temp - (s*(M[IDX(i,max->col)] + (tau*temp)));
    M[IDX(i,max->col)] = M[IDX(i,max->col)] + (s*(temp - (tau*M[IDX(i,max->col)])));
  }
  for(i = max->row + 1; i < max->col; i++){ // Case max->row < i < max->col
    temp = M[IDX(max->row,i)];
    M[IDX(max->row,i)] = temp - (s*(M[IDX(i,max->col)] + (tau*M[IDX(max->row,i)])));
    M[IDX(i,max->col)] = M[IDX(i,max->col)] + (s*(temp - (tau*M[IDX(i,max->col)])));
  }
  for(i = max->col + 1; i < 4; i++){ // Case i > max->col
    temp = M[IDX(max->row,i)];
    M[IDX(max->row,i)] = temp - (s*(M[IDX(max->col,i)] + (tau*temp)));
    M[IDX(max->col,i)] = M[IDX(max->col,i)] + (s*(temp - (tau*M[IDX(max->col,i)])));
  }
  return;
}

__attribute__((target_clones("avx2","sse4.2","default")))
static int
gershgorin(const float *restrict M, const float threshold)
{
  int i,j;
  float total = 0.0f;
  for(i = 0; i < 4; i++){
    total = 0.0f;
    for(j = 0; j < 4; j++){
      if(i == j) continue;
      total += fabsf(M[IDX(i,j)]);
    }
    if((M[IDX(i,i)] - total) < threshold) return 0;
  }
  return 1;
}
