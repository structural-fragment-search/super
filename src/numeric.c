/* This file is part of Super. A 3D pattern-matching program.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file numeric.c
 * \brief Numerical support routines for mathematical functions
 */

#include <numeric.h>

/**
 * \brief Compute the L2 (euclidean) norm of a 3D vector
 */
__attribute__((target_clones("avx2","sse4.2","default")))
float
vector_norm(const struct db_vector *restrict const v)
{
  return sqrtf((v->X * v->X) + (v->Y * v->Y) + (v->Z * v->Z));
}
/*
void
compute_vector_norms(const struct db_vector *restrict vec,
		     float *restrict norm, uint32_t num)
{
  for(const struct db_vector *v = vec; v < vec+num; ++v){
    *norm = vector_norm(v);
    norm++;
  }
}
*/
void
compute_vector_norms(const struct db_vector *restrict vec,
		     float *restrict norm, uint32_t num)
{
  for(unsigned int i = 0; i < num; i++){
    norm[i] = vector_norm(&vec[i]);
  }
}
