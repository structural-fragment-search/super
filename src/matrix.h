/* This file is part of Super. A 3D pattern-matching program.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file matrix.h
 * \brief Implements Kearsleys method and gershgorin circles to compute an RMSD
 */

#include <stdint.h>

#include <vector_set.h>
#include <query_parser.h>

/**
 * \brief Default superposition function: Kearsleys algorithm.
 * \param qry The query set of coordinates
 * \param ref The reference set of coordinates
 * \param rmsd Variable to store the result (RMSD) into
 * \param threshold The RMSD threshold used in computing gershgorin circles
 *        Note that this is NOT the same as the user supplied RMSD threshold
 * \return EINVALID if not within gershgorin bounds
 * \return EMEM if \a data is not pre-allocated
 * \return ENONE on successful superposition
 *
 * Uses the numerically stable Jacobi matrix diagonalisation algorithm
 * to find eigenvalues.
 */
int
superpose(const struct query_data *restrict qry,
          const struct vector_set *restrict ref,
          float *rmsd,
          const float threshold);
