/* This file is part of Super. A 3D pattern-matching program.
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Defines an API for sequential access to a database.
 *
 * Database version 1 (2017)
 * MAGIC NUMBER expects to be: protein database prE-proCEssED vErsion 1.0:
 * 0xED 0xAB 0xAE 0xEC: 0xEDABAEEC
 * Previous database version was version 0 (2011)
 *
 * DATABASE: Privides access to a binary database of 3D structures
 * FORMAT: [HEADER][[Entry Header][Coordinate Data]...]...[[Meta Data]...]
 * Multi-byte data is always in little-endian format
 * HEADER:
 *    offset | size (bytes) | description
 *    =======+==============+===========
 *     0     | 4 (uint32_t) | MAGIC file identification number
 *     4     | 4 (uint32_t) | Version number
 *     8     | 4 (uint32_t) | Number of coordinate entries (entry headers)
 *
 * Entry Header:
 *    offset | size (bytes) | description
 *    =======+==============+============
 *     0     | 4 (4*char)   | 4 character identifier (non-unique)
 *     4     | 4 (uint32_t) | Base index into metadata for this entry
 *     8     | 2 (uint16_t) | Number of coordinates in this entry
 *     10    | 1 (char)     | Chain identifier (unique combined with ID)
 *     11    | 1 (char)     | padding
 *
 * Coordinate Data:
 *    offset | size (bytes)  | description
 *    =======+===============+============
 *      0    | 4 (float)     | Single-precision floating point X coordinate
 *      4    | 4 (float)     | Single-precision floating point Y coordinate
 *      8    | 4 (float)     | Single-precision floating point Z coordinate
 *
 * Meta Data:
 *    offset | size (bytes)  | description
 *    =======+===============+============
 *      0    | 2 (int16_t)   | Residue number
 *      2    | 1 (char)      | Single letter residue identifier
 *      3    | 1 (char)      | Insertion code
 */

/**
 * \file database.h
 * \brief Defines an API for manipulating a pre-processed coordinate database
 */


#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef HAVE_CONFIG_H
 #include <super_config.h>
#endif
#include <serrors.h>

#ifndef DATABASE_H__
#define DATABASE_H__

/**
 * \def MAGIC
 * \brief A value at the beginning of a database file for identification
 * and error checking.
 */
#define MAGIC 0xEDABAEEC


/**
 * \struct db_vector
 * \brief A coordinate data entry
 */
struct db_vector{
  float X; // X coordinate
  float Y; // Y coordinate
  float Z; // Z coordinate
};

/**
 * \struct db_header
 * \brief The global database header
 */
struct db_header{
  uint32_t magic;       // Magic number for error checking
  uint32_t version;     // Version of this database
  uint32_t entry_count; // Number of entries in the database
  uint32_t metadata;    // Offset to the beginning of the metadata
};

/**
 * \struct db_entry_header
 * \brief A database entry header.
 */
struct db_entry_header{
  char code[4];             // Protein Data Bank code for this entry
  uint32_t base_meta_index; // Base index to metadata for this entry
  uint16_t size;            // Number of vectors in this entry
  char chainID;             // Chain ID of this entry
  char padding;             // Padding byte (not used)
};

/**
 * \struct db_meta
 * \brief A metadata entry
 */
struct db_meta{
  int16_t residueID; // The PDB residue identifier
  char slc;          // Single letter residue name (from three-letter name)
  char icode;        // Residue insertion code
};

/**
 * \struct db
 * \brief Database state information, not associated with on-disk data
 */
struct db{
  void* buffer;                   // Buffer containing the file contents
  const struct db_meta* metadata; // Beginning of the metadata area
  size_t buffer_size;             // Total size of the buffer in bytes
  uint32_t entry_count;           // Number of coordinate entries
};

/**
 * \brief Fill the \a database structure from the file named \a db.
 * \param db The name of the database file to load
 * \param database
 *    A pre-allocated structure to store the loaded database
 * \return EINVALID if the file is not regular, or the database has 0 entries
 * \return EEXIST if \a db does not exist
 * \return EMAGIC if the magic number in the loaded database is corrupt
 * \return EMEM if \a database is not already allocated memory, or mmap failed
 * \return ENONE on success
 *
 * Attempts to mmap() the database file.
 * If any error occures, \a db_destroy() should not be called on the 
 * returned \a struct \a db.
 * The \a database should point to a pre-allocated structure which
 * is filled.
 * The \a database structure must be allocated by the user before use of this
 * function.
 */
int
db_load_from_file(const char* db, struct db* database);

/**
 * \brief Clean up \a database data structure
 * \param database The database structure to cleanup
 *
 * This function will perform all internal cleanup operations. However
 * it is the users responsibility to free() the \a database structure itself.
 */
void
db_destroy(struct db* database);

/**
 * \brief Extract a db_meta from the database based on an index
 * \param database The database from which to extract the metadata
 * \param index The offset of the residue (e.g. from a result)
 * \return The metadata associated with the residue at the given index
 */
const struct db_meta*
db_meta_by_index(const struct db* restrict database, uint32_t index);

/**
 * \brief Translates of a range of coordinate vectors to their barrycenter
 *
 * This function is an experimental placeholder for potential fast update
 * \param dest An array for coordinate vectors to be stored into
 * \param src An contiguous array of non-centered coordinate vectors
 * \param number The number of vectors to compute the barrycenter for
 */
void
db_vector_center_initial(struct db_vector *restrict dest,
                         const struct db_vector *restrict src,
                         const uint32_t number);

#endif /* DATABASE_H__ */
