/* This file is part of Super. A 3D pattern-matching program.
 * Command line arguments
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file arguments.c
 * \brief Process command line arguments into a usable form (implementation)
 */

#ifdef HAVE_CONFIG_H
#include <super_config.h>
#endif // HAVE_CONFIG_H
#define _POSIX_C_SOURCE 201109L

#include <arguments.h>
#include <argtable3.h>
#include <stdlib.h>
#include <string.h>

void
parse_arguments(int argc, char** argv, struct arguments* arguments)
{
  struct arg_lit *arg_help, *arg_version, *arg_verb;
  struct arg_int *arg_threads, *arg_coil;
  struct arg_file *arg_query, *arg_database, *arg_out;
  struct arg_dbl *arg_thresh;
  struct arg_end *end;

  void *argtable[] = {
    arg_help = arg_litn("h", "help", 0, 1, "display this help and exit"),
    arg_version = arg_litn(NULL, "version", 0, 1, "Print program version"),
    arg_verb = arg_litn(NULL, "quiet", 0, 1, "Produce no output"),

    arg_threads = arg_intn("c", "thread-count", "COUNT", 0, 1, "Maximum number of threads to run"),
    arg_coil = arg_intn(NULL, "random-coil-length", "VAL", 0, 1, "Length of unknown space between queries"),
    arg_query = arg_filen("q", "query", "QUERY", 0, 2, "Query PDB fragment"),
    arg_out = arg_filen("o", "output", "OUTPUT", 0, 1, "Output to file"),
    arg_thresh = arg_dbln("t", "threshold", "THRESHOLD", 0, 1, "Pattern matching RMSD threshold"),
    arg_database = arg_filen(NULL, NULL, "DATABASE", 1, 1, "Input search database file"),
    end = arg_end(20),
  };

  int nerrors = arg_parse(argc, argv, argtable);

  if(arg_help->count > 0){
    printf("Usage: "PACKAGE);
    arg_print_syntax(stdout, argtable, "\n");
    printf("A 3D pattern search program\n");
    arg_print_glossary(stdout, argtable, " %-25s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(0);
  }

  if(arg_version->count > 0){
    printf("%s\n", PACKAGE_STRING);
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(0);
  }

  if(nerrors > 0){
    arg_print_errors(stdout, end, PACKAGE);
    printf("Try '%s --help' for more information.\n", PACKAGE);
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(1);
  }

  if(arg_verb->count > 0){
    arguments->verbose = 0;
  }

  if(arg_threads->count == 1){
    arguments->threads = (unsigned)arg_threads->ival[0];
  }

  if(arg_coil->count == 1){
    arguments->coil = arg_coil->ival[0];
  }

  if (arg_out->count == 1){
    arguments->output = fopen(arg_out->filename[0], "w");
  }else{
    arguments->output = stdout;
  }

  if(arg_thresh->count == 1){
    arguments->threshold = arg_thresh->dval[0];
    if(arguments->threshold < 0.0) arguments->threshold = 0.0;
  }

  arguments->n_query = arg_query->count;
  if(arguments->n_query > 0){
    arguments->query = calloc(arg_query->count, sizeof(const char*));
    for(int i = 0; i < arg_query->count; i++){
      arguments->query[i] = strdup(arg_query->filename[i]);
    }
  }
  
  arguments->search_database = strdup(arg_database->filename[0]);

  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
}

void
free_arguments_internal(struct arguments* arguments)
{
  if(arguments->output != stdout){
    fclose(arguments->output);
    arguments->output = NULL;
  }
  
  if(arguments->search_database){
    free(arguments->search_database);
    arguments->search_database = NULL;
  }

  if(arguments->query){
    for(unsigned i = 0; i < arguments->n_query; i++){
      free(arguments->query[i]);
    }
    free(arguments->query);
    arguments->query = NULL;
  }

  arguments->threads = 0;
  arguments->coil = 0;
  arguments->n_query = 0;
  arguments->threshold = 0.0f;
}
