/* This file is part of Super. A 3D pattern-matching program.
 * Utility functions
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 201109L

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <util.h>
#include <serrors.h>

/**
 * \file util.c
 * \brief Fast utility functions
 */

int
get_query_path_auto(char **path)
{
#define SUPER_QUERY_FILENAME "/query.pdb"
#define MAX_SEARCH_DIRS 10
  int i = 1, j;
  if(path == NULL) return EMEM;
  const int filename_len = strlen(SUPER_QUERY_FILENAME);
  const char *path_var_env = (const char*)getenv("SUPER_DB_PATH");
  if(!path_var_env) return EEXIST;
  char *path_var = strdup(path_var_env);
  if(!path_var) return EMEM;
  *path = malloc(strlen(path_var) + filename_len + 1);
  if(!(*path)){
    free(path_var);
    return EMEM;
  }
  const char* search_directory[MAX_SEARCH_DIRS];
  struct stat query_file;

  search_directory[0] = strtok(path_var, ":\n");
  do{
    search_directory[i] = strtok(NULL, ":");
    i++;
  }while((i < MAX_SEARCH_DIRS) && (search_directory[i-1] != NULL));

  for(j = 0; j < (i-1); j++){
    int search_path_len = strlen(search_directory[j]);
    memcpy((*path), (const char*)search_directory[j], search_path_len);
    memcpy((*path) + search_path_len, SUPER_QUERY_FILENAME, filename_len);
    (*path)[search_path_len + filename_len] = 0;
    if(!stat(*path, &query_file)){
      free(path_var);
      return ENONE;
    }
  }
  free(path_var);
  free(*path);
  (*path) = NULL;
  return EEXIST;
}
