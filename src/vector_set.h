/* This file is part of Super. A 3D pattern-matching program.
 * Interface to a moving window of coordinates
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * A vector_set is a buffer of vectors stored as 3 sequential
 * single precision floating-point values. Conceptually, a vector_set
 * simply maps part of the database as follows:
 *
 * DATABASE| VECTOR_SET (moving window)
 * |<x,y,z>|
 *  <x,y,z>| ------  \/
 *  <x,y,z>|     |   \/ The vector_set
 *  <x,y,z>|     |   \/ walks in this
 *  <x,y,z>|     |   \/ direction
 *  <x,y,z>| ------  \/
 *  <x,y,z>|
 *  <x,y,z>|
 *  <x,y,z>|
 *  <x,y,z>|
 *  <x,y,z>|
 */

#ifndef __VECTOR_SET_H__
#define __VECTOR_SET_H__

/**
 * \file vector_set.h
 * \brief Manipulation API for vector sets.
 *
 * In \a super, vector sets map a portion (or all) of a database entry.
 * They act as a fixed size (number of vectors/amino-acids) moving window
 * over the coordinates within each entry. They can't, however, map more
 * atoms/vectors than are in a particular entry. If this situation occurs
 * the entry will simply be ignored and the next valid entry will be be mapped.
 */

#include <stdint.h>
#include <stdio.h>
#include <pthread.h>

#ifdef HAVE_CONFIG_H
#include <super_config.h>
#endif //HAVE_CONFIG_H

#include <database.h>

/**
 * \def VECTOR_SET_CLONED
 * \brief Flags this set as a clone and may reference another vector set
 */
#define VECTOR_SET_CLONED 0x1

/**
 * \def VECTOR_SET_DESTROYED
 * \brief Flags that this set has been destroyed
 */
#define VECTOR_SET_DESTROYED 0x2

/**
 * \struct vector_set
 * \brief Maps part of a database to describe a subset of its vectors.
 */
struct vector_set{
  struct db_vector *centered; // Stores the vector set after translation
  
  struct db_vector *set;      // Pointer to a linear array of vectors
  
  uint32_t base_meta_index;   // Meta data offset for current entry
  float factor;               // Convert div to mul factor

  float Xsum;                 // Sum of x-coordinates
  float Ysum;                 // Sum of y-coordinates

  float Zsum;                 // Sum of z-coordinates
  uint32_t vector_count;      // Num vectors mapped by this set
  
  uint32_t current_vector;    // Vector currently beginning at
  uint32_t first_entry;       // First entry that can be mapped
  
  uint32_t last_entry;        // Last entry that can be mapped
  uint32_t current_entry;     // Currently mapped entry
  
  char ID[4];                 // 4 character identifier
  uint16_t entry_size;        // Number of vectors in the current entry
  uint16_t coil_length;       // Length of the coil between query entries

  uint16_t divider;           // Beginning of gap
  uint16_t flags;             // Flags for options
  char chain;                 // Chain ID
};

/**
 * \brief Maps coordinate data from a database into a vector set.
 * \param set
 *    A pre-allocated \a struct \a vector_set to fill.
 * \param db
 *    A database to map
 * \param size
 *    The number of vectors/amino-acids this set must map
 * \param coil_length
 *    Size of any gap between query parts
 * \param divider
 *    The position of any gap between query parts
 * \return EMEM if set or database is not already allocated
 * \return EMEM on memory allocation error
 * \return EOOB if no entry in the database is large enough
 * \return ENONE if the mapping was successful
 *
 * Fills the vector set structure with translated coordinate data from
 * \a db. The size should match the query size, therefore the query
 * should already be loaded.
 */
int
vector_set_create(struct vector_set* set, const struct db* db,
		  const size_t size, const size_t coil_length,
		  const size_t divider);


/**
 * \brief Frees memory associated with \a set.
 * \param set
 *    \a struct \a vector_set to cleanup
 *
 * This function will not free memory used by \a set itself as this
 * structure is owned by the user. However it will clean up internally
 * so that free can be called on \a set later if necessary.
 */
void
vector_set_destroy(struct vector_set* set);


/**
 * \brief Load \a set with the next window from in this entry,
 *        or the start of the next entry.
 * \param set Coordinate vector set mapping to move
 * \return EOOB if an attempt was made to proceed out of a \a restricted region
 *         in the database, or there are no more entries.
 * \return ENONE on success.
 *    
 * Usually steps to the next vector in the current database entry. If the end
 * of an entry is reached, the beginning of the next entry will be mapped.
 * This function will implicitly perform a \a translate_to_com() to translate
 * the set to it's centre-of-mass.
 */
int
vector_set_proceed(struct vector_set *restrict set);

/**
 * \brief Load a specific (future) database entry
 * \param set
 *    The \a vector_set to move
 * \param entryid
 *    The entry to move this set
 * \precondition entryid > set->current_entry
 * \return EEXIST if \a entryid does not exist in the database
 * \return EOOB if \a entryid is larger than the allowable limit
 * \return ENONE on success
 *
 * Always maps the beginning of the entry
 */
int
vector_set_seek(struct vector_set* set, size_t entryid);

/**
 * \brief Creates a deep copy of \a set
 * \param set The vector_set to be cloned
 * !Important note: malloc() is called to allocate the cloned \a vector_set
 *                  the user must eventually call free() on the returned pointer
 *                  because vector_set_destroy() will NOT do so
 * \return a cloned vector set
 * \return NULL on error
 */
struct vector_set*
vector_set_clone(const struct vector_set* set);

/**
 * \brief Restrict the entries this vector set can iterate over
 * \param set The vector set to be restricted
 * \param first The entryID of the start entry of the restriction
 * \param last the entryID of the last entry of the restriction
 * \return ENONE on success
 * \return EOOB if the set is currently or would be out of bounds
 *
 * Performs a \a vector_set_seek() to position the set at the start
 * of the restricted area if the set is currently before the restricted
 * area.
 */
int
vector_set_restrict(struct vector_set* set, uint32_t first, uint32_t last);

#endif /* __VECTOR_SET_H__ */
