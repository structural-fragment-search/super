/* This file is part of Super. A 3D pattern-matching program.
 * Error handling
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file serrors.h
 * \brief Defines error numbers for return values from Super functions.
 */

#ifndef SERRORS_H__
#define SERRORS_H__
#undef EEXIST

/**
 * \def ENONE
 * \brief No error occured.
 */
#define ENONE 0

/**
 * \def EINVALID
 * \brief The operation requested is invalid in this context.
 */
#define EINVALID 1

/**
 * \def EEXIST
 * \brief The operation requested resources that do not exist.
 */
#define EEXIST 2

/**
 * \def EMEM
 * \brief The operation encountered a memory allocation error
 */
#define EMEM 3

/**
 * \def EMAGIC
 * \brief Magic number mismatch, could mean corruption or incorrect
 *        file format.
 */
#define EMAGIC 4

/**
 * \def EOOB
 * \brief The request accesses information that is out of it's allowable region
 */
#define EOOB 5

/**
 * \def EUNKNOWN
 * \brief An unknown error occured
 */
#define EUNKNOWN 6

/**
 * \brief Print a warning prefixed message to stderr about a specified code
 * \param err The error code
 */
void
print_warn_message(int err);

/**
 * \brief Print an error prefixed message to stderr about a specified code
 * \param err The error code
 */
void
print_err_message(int err);

/**
 * \brief Catch all types of error codes and print detailed messages to stderr
 * \param err An error code
 */
void
handle_error(int err);

#endif /* SERRORS_H__ */
