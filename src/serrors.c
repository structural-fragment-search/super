/* This file is part of Super. A 3D pattern-matching program.
 * Error handling
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <serrors.h>
#include <stdio.h>

/**
 * \file serrors.c
 * \brief Defines functions for handling user readable error output
 */

/**
 * Array of strings for verbose error messages
 */
const char*
ew_messages[] = {
  "Success",        // ENONE
  "Invalid",        // EINVALID
  "Does not exist", // EEXIST
  "Memory",         // EMEM
  "Magic number",   // EMAGIC
  "Out of bounds",  // EOOB
  "Unknown"         // EUNKNOWN
};

void
print_warn_message(int err)
{
  fprintf(stderr, "**Warning**: %s\n", ew_messages[err]);
}

void
print_err_message(int err)
{
  fprintf(stderr, "!!ERROR!!: %s\n", ew_messages[err]);
}

void
handle_error(int err)
{
  if(err == ENONE) return;
  print_err_message(err);
  switch(err){
  case EEXIST:
    fprintf(stderr, "One or more of the input files were not found\n");
    break;
  case EINVALID:
    fprintf(stderr, "One or more of the input files are not regular files\n");
    break;
  case EOOB:
    fprintf(stderr, "Query is too large\n");
    break;
  case EMEM:
    fprintf(stderr, "Out of memory or memory allocation error\n");
    break;
  case EUNKNOWN:
    fprintf(stderr, "Could not open one or more of the input files\n");
    break;
  default:
    fprintf(stderr, "An unhandled error occured. This is likely a bug!\n");
    break;
  }
}
