/* This file is part of Super. A 3D pattern-matching program.
 * Interface to a moving window of coordinates
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <super_config.h>
#endif // HAVE_CONFIG_H

#define _DEFAULT_SOURCE

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <assert.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdio.h>
#include <math.h>

#include <vector_set.h>
#include <database.h>
#include <serrors.h>

/**
 * \file vector_set.c
 * \brief Manipulation of coordinate vector sets
 */

/**
 * \brief Compute coordinate component sums for use in centering
 */
static void
compute_center_initial(struct vector_set *restrict set);


/**
 * \brief Update coordinate sums for a single vector increment
 */
static void
compute_center_update(struct vector_set *restrict set);

/**
 * \brief Find the centre-of-mass of of the structure defined by \a set
 * and translate all vectors in \a set.
 *
 * This function incurs set->vector_count*3 memory writes which would be
 * required anyway.
 * This function is destructive to \a set->centered.
 */
static void
translate_to_com(const struct vector_set *restrict set);

/**
 * \brief Loops over database entries until an entry is large enough to map
 * the supplied set
 * Worst case time complexity: O(N), N is the number of entries in the database
 */
static int
next_valid_entry(struct vector_set *restrict set);

int
vector_set_create(struct vector_set* set, const struct db* db,
		  const size_t size, const size_t coil_length,
		  const size_t divider)
{
  int err = ENONE;
  if(!set) return EMEM;

  struct db_entry_header* entry
    = (struct db_entry_header*)((char*)db->buffer
                                + sizeof(struct db_header));
  
  set->centered = malloc(size * sizeof(struct db_vector));
  if(!set->centered){
    return EMEM;
  }
  set->set = (struct db_vector*)((char*)entry
                                 + sizeof(struct db_entry_header));
  set->base_meta_index = entry->base_meta_index;
  set->coil_length = coil_length;
  set->divider = divider;
  set->current_entry = 0;
  set->first_entry = 0;
  set->last_entry = db->entry_count - 1;
  set->current_vector = 0;
  set->flags = 0;
  memcpy(set->ID, entry->code, 4);
  set->entry_size = entry->size;
  set->chain = entry->chainID;
  set->vector_count = size;
  set->factor = 1.0f / (float)set->vector_count;
  set->Xsum = set->Ysum = set->Zsum = 0.0f;

  if(set->vector_count > entry->size){
    err = next_valid_entry(set);
  }

  if(err == ENONE){
    compute_center_initial(set);
    translate_to_com(set);
  }
  
  return err;
}

void
vector_set_destroy(struct vector_set* set)
{
  if(set){
    if(set->centered){
      free(set->centered);
      set->centered = NULL;
    }
    memset(set, 0, sizeof(struct vector_set));
    set->flags = VECTOR_SET_DESTROYED;
  }
}

int
vector_set_proceed(struct vector_set *restrict set)
{
  assert(set);
  int err = ENONE;
  //the current set is (or is about to) overflow the current entry
  //need to go to the next entry that is large enough to contain this set
  if(set->current_vector >= (set->entry_size - set->vector_count)){
    err = next_valid_entry(set);
    compute_center_initial(set);
  }else{
    set->current_vector++;
    compute_center_update(set);
  }

  if(!err){
    translate_to_com(set);
  }
  return err;
}

int
vector_set_seek(struct vector_set* set, size_t entryid)
{
  if(entryid == set->current_entry) return ENONE;
  if(entryid > set->last_entry) return EOOB;
  if(entryid < set->current_entry) return EOOB;
  int err = ENONE;
  while(set->current_entry < entryid){
    err = next_valid_entry(set);
  }
  if(!err){
    compute_center_initial(set);
    translate_to_com(set);
  }
  return err;
}

struct vector_set*
vector_set_clone(const struct vector_set* set)
{
  struct vector_set* cloned;
  if(!set) return NULL;
  if(set->flags & VECTOR_SET_DESTROYED) return NULL;
  cloned = malloc(sizeof(struct vector_set));
  if(!cloned) return NULL;
  memcpy(cloned, set, sizeof(struct vector_set));
  cloned->flags |= VECTOR_SET_CLONED;
  cloned->centered = malloc(set->vector_count*sizeof(struct db_vector));
  if(!cloned->centered){
    free(cloned);
    return NULL;
  }
  return cloned;
}

int
vector_set_restrict(struct vector_set* set, uint32_t first, uint32_t last)
{
  assert(set);
  if(last > set->last_entry) return EOOB;
  if(last < first) return EOOB;
  if(set->current_entry > last) return EOOB;
  if(set->current_entry < first){
    size_t of = set->first_entry, ol = set->last_entry;
    int err = vector_set_seek(set, first);
    if(err){
      set->first_entry = of;
      set->last_entry = ol;
      return err;
    }
  }
  set->first_entry = first;
  set->last_entry = last;
  return ENONE;
}

__attribute__((target_clones("avx2","sse4.2","default")))
static void
compute_center_initial(struct vector_set *restrict set)
{
  uint32_t v_idx = set->current_vector;
  const uint32_t v_end = v_idx + set->vector_count + set->coil_length;
  const uint32_t divider = v_idx + set->divider;
  const struct db_vector *restrict vector = set->set;
  set->Xsum = set->Ysum = set->Zsum = 0.f;

  while(v_idx < divider){
    set->Xsum += vector[v_idx].X;
    set->Ysum += vector[v_idx].Y;
    set->Zsum += vector[v_idx].Z;
    v_idx++;
  }
  // If this is a gapped query: we need to "skip over" the coil area
  v_idx += set->coil_length;
  while(v_idx < v_end){
    set->Xsum += vector[v_idx].X;
    set->Ysum += vector[v_idx].Y;
    set->Zsum += vector[v_idx].Z;
    v_idx++;
  }
}

__attribute__((target_clones("avx2","sse4.2","default")))
static void
compute_center_update(struct vector_set *restrict set)
{
  const uint32_t v_idx = set->current_vector - 1;
  const uint32_t v_end = v_idx + set->vector_count + set->coil_length;
  const uint32_t divider = v_idx + set->divider;
  const uint32_t end_coil = v_idx + set->divider + set->coil_length;
  const struct db_vector *restrict vector = set->set;

  assert(v_idx < set->entry_size);

  //subtract the old first
  set->Xsum -= vector[v_idx].X;
  set->Ysum -= vector[v_idx].Y;
  set->Zsum -= vector[v_idx].Z;

  if(set->coil_length > 0){
    //add on vector at divider
    set->Xsum += vector[divider].X;
    set->Ysum += vector[divider].Y;
    set->Zsum += vector[divider].Z;

    //subtract at end of coil
    set->Xsum -= vector[end_coil].X;
    set->Ysum -= vector[end_coil].Y;
    set->Zsum -= vector[end_coil].Z;
  }

  //add on the end one
  set->Xsum += vector[v_end].X;
  set->Ysum += vector[v_end].Y;
  set->Zsum += vector[v_end].Z;
}

__attribute__((target_clones("avx2","sse4.2","default")))
static void
translate_to_com(const struct vector_set *restrict set)
{
  const float Xav = set->Xsum * set->factor;
  const float Yav = set->Ysum * set->factor;
  const float Zav = set->Zsum * set->factor;
  uint32_t v_idx = set->current_vector;
  const struct db_vector *restrict end
    = &set->set[v_idx + set->vector_count + set->coil_length];
  const struct db_vector *restrict divider
    = &set->set[v_idx + set->divider];
  const struct db_vector *restrict vector = &set->set[v_idx];
  struct db_vector *restrict centered = set->centered;

  while(vector < divider){
    centered->X = vector->X - Xav;
    centered->Y = vector->Y - Yav;
    centered->Z = vector->Z - Zav;
    vector++; centered++;
  }
  // If this is a gapped query: we need to "skip over" the coil area
  vector += set->coil_length;
  while(vector < end){
    centered->X = vector->X - Xav;
    centered->Y = vector->Y - Yav;
    centered->Z = vector->Z - Zav;
    vector++; centered++;
  }
}

static int
next_valid_entry(struct vector_set *restrict set)
{
  assert(set);
  struct db_entry_header* header;
  if(set->current_entry == set->last_entry) return EOOB;

  do{
    header = (struct db_entry_header*)((char*)set->set
                                       + (set->entry_size
                                          * sizeof(struct db_vector)));
    set->set = (struct db_vector*)((char*)header
				   + sizeof(struct db_entry_header));
    set->entry_size = header->size;
    set->current_entry++;
    if(set->current_entry > set->last_entry){
      return EOOB;
    }
  }while(header->size < set->vector_count);
    
  set->base_meta_index = header->base_meta_index;
  set->current_vector = 0;
  memcpy(set->ID, header->code, 4);
  set->entry_size = header->size;
  set->chain = header->chainID;
  return ENONE;
}
