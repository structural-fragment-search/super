#!/bin/bash

# Generate 100 random test queries of varying length from the PDB

if [ $# -ne 3 ]; then
    echo "You need to supply a directory containing PDB files, the number of queries and the filname suffix to generate!"
    echo "./generate-test-queries.sh /path/to/pdb 1000 .ent.gz"
    exit
fi

PDB_PP="${HOME}/install/bin/pdb_pp.py"

PDB_DIR=$1
GEN_N=$2
SUFFIX=$3
PDB_LIST="file_list.txt"
RANDOM_LIST="random.txt"
HISTOGRAM="histogram.txt"
COUNTER=0
CAT=zcat

function get_random_line {
    local rand=`cat /proc/sys/kernel/random/uuid | cut -c1-4 | od -d | head -1 | cut -d' ' -f2`
    local lines=`cat "$1" | wc -l`
    local line=`expr $rand % $lines + 1`
    head -$line $1 | tail -1
}

find ${PDB_DIR} -name '*'${SUFFIX} > ${PDB_LIST}
echo "Done listing..."
while [ $COUNTER -lt ${GEN_N} ]; do
    echo -e -n "Creating query: ${COUNTER}\r"
    FILE=$(get_random_line ${PDB_LIST})
    PDBID=`basename ${FILE} ${SUFFIX} | tr '[:lower:]' '[:upper:]' | sed 's/^PDB//'`
    LENGTH=$[ ( RANDOM % 46 ) + 5 ]
    ATOMS=`${CAT} ${FILE} | grep ^ATOM | grep " CA " | wc -l`
    REPEATS=`${CAT} ${FILE} | grep ^ATOM | grep " CA " | tr -s ' ' | cut -d' ' -f 2 | grep -n ^2$`
    if [ `echo ${REPEATS} | tr ' ' '\n'| wc -l` -ne 1 ]; then
	ATOMS=`echo ${REPEATS} | tr ' ' '\n' | head -2 | tail -1 | cut -d: -f1`
	let ATOMS=${ATOMS}-1
    fi

    if [ $ATOMS -lt $LENGTH ]; then
	continue;
    fi
    OFFSET=$[ ( ( RANDOM % ${ATOMS} ) + 1 ) / 2 ]
    let END=$LENGTH+$OFFSET
    if [ $END -gt $ATOMS ]; then
	continue;
    fi

    let INDEX=${OFFSET}-1
    echo "query${COUNTER}.pdb is ${PDBID} ${INDEX} ${LENGTH}" >> query_list_`date +%F`
    echo $LENGTH >> ${HISTOGRAM}
    FIRST=`${CAT} ${FILE} | grep -n ^ATOM | grep " CA " | tail -n +$OFFSET | head -1 | cut -d' ' -f 1 | cut -d: -f 1`
    ${CAT} ${FILE} | head -1 > query${COUNTER}.pdb
    ${CAT} ${FILE} | tail -n +${FIRST} | grep ^ATOM | grep " CA " | head -$LENGTH >> query${COUNTER}.pdb
    $PDB_PP -q -o query${COUNTER}.db query${COUNTER}.pdb > /dev/null
    rm query${COUNTER}.pdb
    let COUNTER=$COUNTER+1
done
echo -e "Length\t| frequency\n========|==========" | tee -a ${HISTOGRAM}.grph
cat ${HISTOGRAM} | awk 'NF > 0 {counts[$0] = counts[$0]+1;} END {for(number in counts) print number,"\t|",counts[number];}' | sort -g -k 1 | tee -a ${HISTOGRAM}.grph
rm ${PDB_LIST} ${HISTOGRAM}
