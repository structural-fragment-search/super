#!/bin/bash
N=1
#for N in `seq 0 50`; do
while [ ${N} -le 50 ]; do
	L=`cat rmsd.${N} | wc -l`
	RandLine=$[ ( RANDOM % ${L} ) + 1 ]
	RMSD=`head -$RandLine rmsd.${N} | tail -1`
	id=$(echo ${RMSD} | cut -d' ' -f 1)
	offset=$(echo ${RMSD} | cut -d' ' -f 2)
	rmsd=$(echo ${RMSD} | cut -d' ' -f 5)
	
	UB=`cat upperbnd.${N} | tr '\t' ' ' | grep "${id} ${offset}"`
	if [ -n "${UB}" ]; then
		idx=`echo ${UB} | cut -d' ' -f 1`
		upbnd=`echo ${UB} | cut -d' ' -f 6`
		echo -e "${id}\t${offset}\t${rmsd}\t${upbnd}\t(${idx})"
		let N=${N}+1
	elif [ ${L} -eq 1 ]; then
		let N=${N}+1
	else
		echo "${L} ..."
	fi
done
		
		
