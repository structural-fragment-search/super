#!/bin/bash
#Tabulates averaged results, this does not act on search data
if [ ! -d tables ]; then
    rm -f tables;
    mkdir tables;
fi


for fname in super.[1-9]*; do
    n1=$(head -1 ${fname} | cut -f 1);
    n2=$(head -1 ${fname} | cut -f 2);
    tname=$(echo ${n1}+${n2});
    if [ ! -f ${tname} ]; then
	echo -e "Threshold,Mean,Min,Max,Median,Mem,%Accepted,%Rejected" > tables/${tname};
    fi
    tail -1 $fname >> tables/${tname}.1;
done

cd tables/;
for name in *.1; do
    orig=$(echo $name | sed 's/.1//');
    cat ${orig} ${name} > ${orig}.tsv;
    rm ${name} ${orig};
done

for f in *.tsv; do
gnuplot<<EOC
set terminal png
set key below
set xlabel "Threshold (angstroms)"
set ylabel "Runtime (seconds)"

set output ${f}.png
plot ${f} using 1:2 with linesp title "Mean",\
     ${f} using 1:3 with linesp title "Minimum",\
     ${f} using 1:4 with linesp title "Maximum",\
     ${f} using 1:5 with linesp title "Median"
EOC