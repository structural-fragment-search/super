#!/bin/bash
#$ -l h_rt=1:30:00
#$ -l h_vmem=2G

if [ $# -lt 4 ]; then
    echo "testrunner.sh PLUGIN THRESHOLD FLAG"
    exit
fi

###
# Arguments
###
PLUGIN=${1}
THRESHOLD=${2}
FLAG=${3} #Flags (-g, -n, -r, -d, --raw-input, --qcprot)

###
# Variables
###
SUPER="./super"
QRY_DIR=${PWD}/queries/
DATABASE=""

COUNTER=0

AVG=0.0
BEST=9999999.9
WORST=0.0
MEM=0
ACCEPTED=0
REJECTED=0

float_scale=2

###
# Code
###
if [ "${FLAG}" = "-g" ]; then
    PARAM="no-gershgorin";
    DATABASE="pdb.db";
elif [ "${FLAG}" = "-n" ]; then
    PARAM="no-full-rmsd";
    DATABASE="pdb.db";
elif [ "${FLAG}" = "-r" ]; then
    PARAM="read";
    DATABASE="pdb.db";
elif [ "${FLAG}" = "-d" ]; then
    PARAM="default";
    DATABASE="pdb.db";
elif [ "${FLAG}" = "--raw-input" ]; then
    PARAM="raw"
    DATABASE="pdb/";
else
    echo "Unknown flag!"
fi

function float_eval()
{
    local stat=0
    local result=0.0
    if [[ $# -gt 0 ]]; then
        result=$(echo "scale=$float_scale; $*" | bc -q 2>/dev/null)
        stat=$?
        if [[ $stat -eq 0  &&  -z "$result" ]]; then stat=1; fi
    fi
    echo $result
    return $stat
}

function float_cond()
{
    local cond=0
    if [[ $# -gt 0 ]]; then
        cond=$(echo "$*" | bc -q 2>/dev/null)
        if [[ -z "$cond" ]]; then cond=0; fi
        if [[ "$cond" != 0  &&  "$cond" != 1 ]]; then cond=0; fi
    fi
    local stat=$((cond == 0))
    return $stat
}



#time output format:
#user-time minor-faults page-size(bytes)

echo -e "${PLUGIN}\t${PARAM}"
for qry in ${QRY_DIR}/query*.db; do
    let COUNTER=${COUNTER}+1
    rm -f log
    echo -e "${PLUGIN}\t${PARAM}" | bzip2 --best >>output.bz2
    LD_LIBRARY_PATH=. /usr/bin/time -f "%U %R %Z" -o log \
	$SUPER -c 1 -l $PLUGIN -t $THRESHOLD ${FLAG} -q $qry ${DATABASE} 2>>stats.run | bzip2 --best >>output.bz2
    R=${?}
    if [ $R -ne 0 ]; then
	echo "Super returned with exit status: ${R}"
    fi
    echo | bzip2 --best >>output.bz2
    
    TIME=`cat log | cut -d' ' -f 1`
    MF=`cat log | cut -d' ' -f 2`
    PS=`cat log | cut -d' ' -f 3`
    echo "(${MF}*${PS})/1048576" | bc >> memuse #1024*1024 = 1048576 Bytes -> MiB
    echo $TIME >> median
    AVG=$(float_eval "$AVG+$TIME")
    if float_cond "$TIME < $BEST"; then
	BEST=$(float_eval "$TIME")
    fi
    if float_cond "$TIME > $WORST"; then
	WORST=$(float_eval "$TIME")
    fi
done
MEM=`cat memuse | awk -v nq=${COUNTER} '{mrss+=$1} END {print mrss/nq}'`
cat stats.run | awk -v nq=${COUNTER} '{matches+=$1; potentials+= $4; total+=$7} END {print ((total-potentials)/total)*nq, (matches/potentials)*nq}' > sts
ACCEPTED=`cat sts | cut -d' ' -f 2`
REJECTED=`cat sts | cut -d' ' -f 1`
MIDDLE=`${COUNTER}/2' | bc`
MEDIAN=`cat median | sort -g | sed -n '${MIDDLE}p'`
AVG=$(float_eval "$AVG / ${COUNTER}")
echo "${THRESHOLD},${AVG},${BEST},${WORST},${MEDIAN},${MEM},${ACCEPTED},${REJECTED}"

rm -f log median sts memuse
