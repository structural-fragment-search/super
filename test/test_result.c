/* This file is part of Super, a 3D vector superposition pattern-matching program.
 * This program is for unit testing the vector_set interface
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>

#include <check.h>

#include <result.h>
#include <database.h>
#include <vector_set.h>
#include <query_parser.h>

#ifndef TEST_DATA_PATH
#define TEST_DATA_PATH "./test/data"
#endif


/***************CHECKS*****************/
START_TEST (create_result_list)
{
  struct result list;
  struct result next, next_next;

  memset(&list, 0, sizeof(struct result));
  memset(&next, 0, sizeof(struct result));
  next.size = 10;
  next.rmsd = 1.0f;
  next.chain = 'A';

  memset(&next_next, 0, sizeof(struct result));
  next_next.size = 20;
  next_next.rmsd = 2.0f;
  next_next.chain = 'B';

  result_list_append(&list, &next);
  fail_if(list.next != &next);
  fail_if(list.next->size != 10);
  fail_if(list.next->rmsd != 1.0f);
  fail_if(list.next->chain != 'A');

  result_list_append(&list, &next_next);
  fail_if(list.next->next != &next_next);
  fail_if(list.next->next->size != 20);
  fail_if(list.next->next->rmsd != 2.0f);
  fail_if(list.next->next->chain != 'B');
}
END_TEST

START_TEST (destroy_result_list)
{
  struct result* list = calloc(1, sizeof(struct result));
  struct result* next = calloc(1, sizeof(struct result));
  struct result* next_next = calloc(1, sizeof(struct result));

  result_list_append(list, next);
  fail_if(list->next != next);

  result_list_append(list, next_next);
  fail_if(list->next->next != next_next);

  result_list_destroy(&list);
  fail_if(list != NULL);
}
END_TEST

START_TEST (print_result_list)
{
  int err;
  struct arguments args;
  struct result* result;
  struct query_data query;
  struct db db;
  struct vector_set vs;
  const char** filename = calloc(2, sizeof(char*));
  filename[0] = TEST_DATA_PATH"/ts2qry.pdb";
  filename[1] = TEST_DATA_PATH"/ts3qry.pdb";
  args.output = tmpfile();
  args.coil = 10;

  err = query_parse(&query, filename, 2);
  fail_if(err != ENONE);

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, query.size, 10, query.divider);
  fail_if(err != ENONE);

  result = calloc(1, sizeof(struct result));
  strncpy(result->ID, "1HHO", 4);
  result->meta_offset = 0;
  result->size = query.size + 10;
  result->rmsd = 1.001f;
  result->chain = 'A';

  result->next = calloc(1, sizeof(struct result));
  strncpy(result->next->ID, "1JHC", 4);
  result->next->meta_offset = 1;
  result->next->size = query.size + 10;
  result->next->rmsd = 2.002f;
  result->next->chain = 'B';

  err = result_list_print(result, &db, &query, &args);
  fail_if(err != ENONE);
  
  rewind(args.output);
  //check output
#define OUT_SIZE (2*(84+1))
  char* output = malloc(OUT_SIZE + 1);
  size_t count = fread(output, 1, OUT_SIZE, args.output);
  output[OUT_SIZE] = 0;
  fail_if(count != OUT_SIZE);

  const char* compare
    = "1HHO\tA\t   1_:30_\t1.001\tVLSPADKTNVKAAWGKVGAHAGEYGAEALE"
    ":GAEGAEGAEG----------GAEGAEGAEG\n"
    "1JHC\tB\t   2_:31_\t2.002\tLSPADKTNVKAAWGKVGAHAGEYGAEALER"
    ":GAEGAEGAEG----------GAEGAEGAEG\n";
  fail_if(strncmp(compare, output, OUT_SIZE) != 0);

  free(output);
  result_list_destroy(&result);
  fail_if(result != NULL);
  db_destroy(&db);
  query_destroy(&query);
  vector_set_destroy(&vs);
  free(filename);
  fclose(args.output);
}
END_TEST

Suite*
super_suite(void)
{
  Suite* s = suite_create("Result List");

  TCase* tc_matrix = tcase_create("Results");
  tcase_add_test(tc_matrix, create_result_list);
  tcase_add_test(tc_matrix, destroy_result_list);
  tcase_add_test(tc_matrix, print_result_list);
  suite_add_tcase(s, tc_matrix);

  return s;
}

int main()
{
  int number_failed;
  Suite* s = super_suite();
  SRunner* sr = srunner_create(s);
  srunner_run_all(sr, CK_VERBOSE);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
