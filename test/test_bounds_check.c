/* This file is part of Super. A 3D pattern-matching program.
 * This program is for unit testing the vector_set interface
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>

#include <check.h>

#include <fast_bounds_check.h>
#include <database.h>
#include <vector_set.h>
#include <query_parser.h>

#ifndef TEST_DATA_PATH
#define TEST_DATA_PATH "./test/data"
#endif


/***************CHECKS*****************/
START_TEST (bounds_check)
{
  int err;
  struct query_data query;
  struct db db;
  struct vector_set vs;
  const char** filename = calloc(2, sizeof(char*));
  filename[0] = TEST_DATA_PATH"/ts2qry.pdb";
  filename[1] = TEST_DATA_PATH"/ts3qry.pdb";

  err = query_parse(&query, filename, 2);
  fail_if(err != ENONE);

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, query.size, 10, query.divider);
  fail_if(err != ENONE);
  

  //correct RMSD = 4.96504f
  // lower bound = 3.5208985
  float lb = default_lower_bound(vs.centered, query.norm, query.size);
  fail_if(fabsf(lb - 3.5208985f) > 0.000001);

  free(filename);
}
END_TEST

Suite*
super_suite(void)
{
  Suite* s = suite_create("Bounds Check");

  TCase* tc_bounds = tcase_create("Bounds");
  tcase_add_test(tc_bounds, bounds_check);
  suite_add_tcase(s, tc_bounds);

  return s;
}

int main()
{
  int number_failed;
  Suite* s = super_suite();
  SRunner* sr = srunner_create(s);
  srunner_run_all(sr, CK_VERBOSE);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
