/* This file is part of Super, a 3D vector superposition pattern-matching program.
 * This program is for unit testing the vector_set interface
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>

#include <check.h>

#include <matrix.h>
#include <database.h>
#include <vector_set.h>
#include <query_parser.h>

#ifndef TEST_DATA_PATH
#define TEST_DATA_PATH "./test/data"
#endif


/***************CHECKS*****************/
START_TEST (basic_rmsd_contig)
{
  int err;
  float rmsd;
  struct query_data query;
  struct db db;
  struct vector_set vs;
  const char** filename = malloc(sizeof(char*));
  filename[0] = TEST_DATA_PATH"/ts1qry.pdb";

  err = query_parse(&query, filename, 1);
  fail_if(err != ENONE);

  err = db_load_from_file(TEST_DATA_PATH"/test1.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, query.size, 0, query.divider);
  fail_if(err != ENONE);

  err = superpose(&query, &vs, &rmsd, 100.0f);
  fail_if(err != ENONE);
  fail_if(fabsf(rmsd - 3.034566f) > 0.00001);
}
END_TEST

START_TEST (basic_rmsd_gap)
{
  int err;
  float rmsd;
  struct query_data query;
  struct db db;
  struct vector_set vs;
  const char** filename = calloc(2, sizeof(char*));
  filename[0] = TEST_DATA_PATH"/ts2qry.pdb";
  filename[1] = TEST_DATA_PATH"/ts3qry.pdb";

  err = query_parse(&query, filename, 2);
  fail_if(err != ENONE);

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, query.size, 10, query.divider);
  fail_if(err != ENONE);

  err = superpose(&query, &vs, &rmsd, 100.0f);
  fail_if(err != ENONE);
  fail_if(fabsf(rmsd - 4.96504f) > 0.00001);
}
END_TEST

START_TEST (rmsd_gershgorin)
{
  int err;
  float rmsd, gershgorin_bound;
  struct query_data query;
  struct db db;
  struct vector_set vs;
  const char** filename = malloc(sizeof(char*));
  filename[0] = TEST_DATA_PATH"/ts1qry.pdb";

  err = query_parse(&query, filename, 1);
  fail_if(err != ENONE);

  err = db_load_from_file(TEST_DATA_PATH"/test1.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, query.size, 0, query.divider);
  fail_if(err != ENONE);

  //Expected RMSD is 3.034566f
  gershgorin_bound = (3.0f * 3.0f) / query.size;
  err = superpose(&query, &vs, &rmsd, gershgorin_bound);
  fail_if(err != EINVALID);
}
END_TEST

START_TEST (small_rmsd)
{
  int err;
  float rmsd;
  struct query_data query;
  struct db db;
  struct vector_set vs;
  const char** filename = malloc(sizeof(char*));
  filename[0] = TEST_DATA_PATH"/ts1qry.pdb";

  err = query_parse(&query, filename, 1);
  fail_if(err != ENONE);

  err = db_load_from_file(TEST_DATA_PATH"/test2.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, query.size, 0, query.divider);
  fail_if(err != ENONE);

  err = superpose(&query, &vs, &rmsd, 0.1f);
  fail_if(err != ENONE);
  printf("%f\n", rmsd);
  fail_if(fabsf(rmsd - 0.0f) > 0.01);
}
END_TEST

Suite*
super_suite(void)
{
  Suite* s = suite_create("Super Matrix");

  TCase* tc_matrix = tcase_create("Matrix");
  tcase_add_test(tc_matrix, basic_rmsd_contig);
  tcase_add_test(tc_matrix, basic_rmsd_gap);
  tcase_add_test(tc_matrix, rmsd_gershgorin);
  tcase_add_test(tc_matrix, small_rmsd);
  suite_add_tcase(s, tc_matrix);

  return s;
}

int main()
{
  int number_failed;
  Suite* s = super_suite();
  SRunner* sr = srunner_create(s);
  srunner_run_all(sr, CK_VERBOSE);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
