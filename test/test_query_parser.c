/* This file is part of Super, a 3D vector superposition pattern-matching program.
 * This program is for unit testing the database interface
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>

#include <check.h>

#include <query_parser.h>

#ifndef TEST_DATA_PATH
#define TEST_DATA_PATH "./test/data"
#endif


/***************CHECKS*****************/
START_TEST (create_query_data_contig)
{
  struct query_data query;
  int err;
  const char** fname = malloc(sizeof(char*));
  fname[0] = TEST_DATA_PATH"/stdqry.pdb";

  err = query_parse(&query, fname, 1);
  fail_if(err != ENONE);

  fail_if(query.size != 15);
  fail_if(query.divider != 0);
  fail_if(query.centered == NULL);
  fail_if(query.sequence == NULL);
  fail_unless(strncmp(query.sequence, "YGAEALERMFLSFPT", query.size) == 0,
              "Sequence is incorrect");
  free(fname);

  query_destroy(&query);
  fail_if(query.centered != NULL);
  fail_if(query.sequence != NULL);
  fail_if(query.size != 0);
  fail_if(query.divider != 0);
}
END_TEST


START_TEST (create_query_two_parts)
{
  struct query_data query;
  int err;

  const char** fname = malloc(2 * sizeof(char*));
  fname[0] = TEST_DATA_PATH"/2ic7_gapped_query_part1.pdb";
  fname[1] = TEST_DATA_PATH"/2ic7_gapped_query_part2.pdb";

  err = query_parse(&query, fname, 2);
  fail_if(err != ENONE);

  fail_if(query.size != 19);
  fail_if(query.divider != 9);
  fail_if(query.centered == NULL);
  fail_if(query.sequence == NULL);
  fail_unless(strncmp(query.sequence, "IGHNVWIGGDNAVIASGAV", query.size) == 0,
              "Sequence is incorrect");

  free(fname);

  query_destroy(&query);
  fail_if(query.centered != NULL);
  fail_if(query.sequence != NULL);
  fail_if(query.size != 0);
  fail_if(query.divider != 0);
}
END_TEST

START_TEST (create_query_exotic_sequence)
{
  struct query_data query;
  int err;
  const char** fname = malloc(sizeof(char*));
  fname[0] = TEST_DATA_PATH"/exotic.pdb";

  err = query_parse(&query, fname, 1);
  fail_if(err != ENONE);

  fail_if(query.size != 15);
  fail_if(query.divider != 0);
  fail_if(query.centered == NULL);
  fail_if(query.sequence == NULL);
  fail_unless(strncmp(query.sequence, "OCUEUQEBMZQKZJX", query.size) == 0,
              "Sequence is incorrect");
  free(fname);

  query_destroy(&query);
  fail_if(query.centered != NULL);
  fail_if(query.sequence != NULL);
  fail_if(query.size != 0);
  fail_if(query.divider != 0);
}
END_TEST

START_TEST (create_query_null)
{
  struct query_data query;
  int err;

  const char** fname = malloc(sizeof(char*));
  fname[0] = TEST_DATA_PATH"/stdqry.pdb";

  err = query_parse(NULL, fname, 1);
  fail_if(err != EMEM);

  err = query_parse(&query, NULL, 1);
  fail_if(err != EMEM);

  free(fname);
}
END_TEST

START_TEST (create_query_zero)
{
  struct query_data query;
  int err;

  const char** fname = malloc(sizeof(char*));
  fname[0] = TEST_DATA_PATH"/stdqry.pdb";

  err = query_parse(&query, fname, 0);
  fail_if(err != EINVALID);

  free(fname);
}
END_TEST

START_TEST (clone_query_contig)
{
  struct query_data query;
  struct query_data* clone;
  int err;

  const char** fname = malloc(sizeof(char*));
  fname[0] = TEST_DATA_PATH"/stdqry.pdb";

  err = query_parse(&query, fname, 1);
  fail_if(err != ENONE);
  free(fname);

  clone = query_clone(&query);
  fail_if(!clone);
  fail_if(query.centered == clone->centered);
  fail_if(query.sequence == clone->sequence);
  
  fail_if(clone->size != 15);
  fail_if(clone->divider != 0);
  fail_if(clone->centered == NULL);
  fail_if(clone->sequence == NULL);
  fail_unless(strncmp(clone->sequence, "YGAEALERMFLSFPT", clone->size) == 0,
              "Sequence is incorrect");

  query_destroy(clone);
  free(clone);
  query_destroy(&query);
}
END_TEST

START_TEST (create_query_noexist)
{
  struct query_data query;
  int err;
  const char** fname = malloc(sizeof(char*));
  fname[0] = TEST_DATA_PATH"/nonexistent.file";

  err = query_parse(&query, fname, 1);
  fail_if(err != EEXIST);
  free(fname);
}
END_TEST

START_TEST (create_query_notafile)
{
  struct query_data query;
  int err;
  const char** fname = malloc(sizeof(char*));
  fname[0] = TEST_DATA_PATH;

  err = query_parse(&query, fname, 1);
  fail_if(err != EINVALID);
  free(fname);
}
END_TEST

START_TEST (create_query_toolarge)
{
  struct query_data query;
  int err;
  const char** fname = malloc(sizeof(char*));
  fname[0] = TEST_DATA_PATH"/1HHO.pdb";

  err = query_parse(&query, fname, 1);
  fail_if(err != EOOB);
  free(fname);
}
END_TEST

START_TEST (create_query_noatomdata)
{
  struct query_data query;
  int err;
  const char** fname = malloc(sizeof(char*));
  fname[0] = TEST_DATA_PATH"/noatomdata.pdb";

  err = query_parse(&query, fname, 1);
  fail_if(err != EINVALID);
  free(fname);
}
END_TEST

Suite*
super_suite(void)
{
  Suite* s = suite_create("Super Query Parser");

  TCase* tc_parser = tcase_create("Parser");
  tcase_add_test(tc_parser, create_query_data_contig);
  tcase_add_test(tc_parser, create_query_two_parts);
  tcase_add_test(tc_parser, create_query_exotic_sequence);
  tcase_add_test(tc_parser, create_query_null);
  tcase_add_test(tc_parser, create_query_zero);
  tcase_add_test(tc_parser, create_query_noexist);
  tcase_add_test(tc_parser, create_query_notafile);
  tcase_add_test(tc_parser, create_query_toolarge);
  tcase_add_test(tc_parser, create_query_noatomdata);
  suite_add_tcase(s, tc_parser);

  TCase* tc_query = tcase_create("Query");
  tcase_add_test(tc_query, clone_query_contig);
  suite_add_tcase(s, tc_query);

  return s;
}

int main()
{
  int number_failed;
  Suite* s = super_suite();
  SRunner* sr = srunner_create(s);
  srunner_run_all(sr, CK_VERBOSE);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
