#! /bin/sh
# Run python unit tests
#
# Copyright (C) 2017, James Collier <james.collier412@gmail.com>
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.

from superdatabase.db import SuperDatabase, VERSION_1_MAGIC
from superdatabase.application import SuperDBApp
import unittest
import unittest.mock
import os
import sys
import struct
from io import StringIO
import ast

ENTRY_HEADER_STRINGS = ["2IC7-A\t[185]", "2IC7-B\t[185]", "2IC7-C\t[185]"]


class DatabaseCreateTest(unittest.TestCase):
    def setUp(self):
        self.datafile = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                 "data", "1HHO.pdb")
        
    def test_entries(self):
        database = SuperDatabase()
        datagen = database.load_from_files([self.datafile])
        data = list(datagen)
        self.assertEqual(len(data), 2)
        chain = data[0]
        crd = chain[0]
        self.assertEqual(len(crd), 141)
        meta = chain[1]
        self.assertEqual(len(meta), 141)
        self.assertEqual(meta[0], (1, 'V'.encode('utf-8'), '_'.encode('utf-8')))
        firstcrd = crd[0]
        self.assertAlmostEqual(5.776, firstcrd[0], places=3)
        self.assertAlmostEqual(17.899, firstcrd[1], places=3)
        self.assertAlmostEqual(5.595, firstcrd[2], places=3)

        chain = data[1]
        crd = chain[0]
        self.assertEqual(len(crd), 146)
        meta = chain[1]
        self.assertEqual(len(meta), 146)
        self.assertEqual(meta[0], (1, 'V'.encode('utf-8'), '_'.encode('utf-8')))
        firstcrd = crd[0]
        self.assertAlmostEqual(10.737, firstcrd[0], places=3)
        self.assertAlmostEqual(-18.8, firstcrd[1], places=3)
        self.assertAlmostEqual(-3.82, firstcrd[2], places=3)


class DatabaseAPITest(unittest.TestCase):
    def setUp(self):
        self.datafile = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                     "data", "test.sdb")
        datapath = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                "..", "examples", "database", "2ic7.pdb.gz")
        infiles = []
        if os.path.isdir(datapath):
            for base, _, files in os.walk(datapath):
                for pdbfile in files:
                    infiles.append(os.path.join(base, pdbfile))
        else:
            infiles = [datapath]
        self.database = SuperDatabase()
        gen = self.database.load_from_files(infiles)
        self.database.write(self.datafile, gen)

    def tearDown(self):
        os.remove(self.datafile)

    def test_written(self):
        self.assertTrue(os.path.isfile(self.datafile))
        statinfo = os.stat(self.datafile)
        self.assertEqual(statinfo.st_size, 8932)
        with open(self.datafile, mode='rb') as datain:
            data = datain.read(16)
            magic, version, ecount, metaoffset = struct.unpack("<IIII", data)
            self.assertEqual(magic, VERSION_1_MAGIC)
            self.assertEqual(version, 1)
            self.assertEqual(ecount, 3)
            self.assertEqual(metaoffset, 6712)
            

    def test_header(self):
        header = self.database.header
        self.assertEqual(header['magic'], VERSION_1_MAGIC)
        self.assertEqual(header['version'], 1)
        self.assertEqual(header['entry_count'], 3)
        self.assertEqual(header['metadata'], 6712)

    def test_entries(self):
        entry_headers = self.database.entries()
        self.assertEqual(len(entry_headers), 3)
        for i in range(0, 2):
            self.assertEqual(entry_headers[i], ENTRY_HEADER_STRINGS[i])

class CLItest(unittest.TestCase):
    def setUp(self):
        self.datafile = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                     "data", "test.sdb")
        datapath = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                "..", "examples", "database", "2ic7.pdb.gz")
        infiles = []
        if os.path.isdir(datapath):
            for base, _, files in os.walk(datapath):
                for pdbfile in files:
                    infiles.append(os.path.join(base, pdbfile))
        else:
            infiles = [datapath]
        database = SuperDatabase()
        gen = database.load_from_files(infiles)
        database.write(self.datafile, gen)

    def tearDown(self):
        os.remove(self.datafile)

    def test_show_db(self):
        cli = SuperDBApp(self.datafile)

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("show db"))
        self.assertEqual(self.datafile+"	[0]",
                         fake_output.getvalue().strip())

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("load"))
        self.assertEqual("Success", fake_output.getvalue().strip())

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("show db"))
        self.assertEqual(self.datafile+"	[3]",
                         fake_output.getvalue().strip())

    def test_show_entry(self):
        cli = SuperDBApp(self.datafile)

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            cli.onecmd("show entry 0")
        self.assertEqual("Unknown entry index: 0",
                         fake_output.getvalue().strip())

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("load"))
        self.assertEqual("Success", fake_output.getvalue().strip())

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("show entry 0"))
        entry_dict = ast.literal_eval(fake_output.getvalue().strip())
        self.assertEqual(entry_dict['size'], 185)
        self.assertEqual(entry_dict['metaIndex'], 0)
        self.assertEqual(entry_dict['chain'], b'A')
        self.assertEqual(entry_dict['name'], b'2IC7')

    def test_show_entries(self):
        cli = SuperDBApp(self.datafile)

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("show entries"))
        self.assertEqual("", fake_output.getvalue().strip())

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("load"))
        self.assertEqual("Success", fake_output.getvalue().strip())

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("show entries"))
        self.assertEqual('\n'.join(ENTRY_HEADER_STRINGS),
                         fake_output.getvalue().strip())

    def test_load(self):
        cli = SuperDBApp("noexist.sdb")

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("load"))
        self.assertEqual("No database to load", fake_output.getvalue().strip())

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("load reallynoexist.sdb"))
        self.assertEqual("No database to load", fake_output.getvalue().strip())

        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertFalse(cli.onecmd("load "+self.datafile))
        self.assertEqual("Success", fake_output.getvalue().strip())
    
    def test_quit(self):
        cli = SuperDBApp(self.datafile)
        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_output:
            self.assertTrue(cli.onecmd("quit"))
        self.assertEqual("Bye", fake_output.getvalue().strip())

if __name__ == "__main__":
    unittest.main()
