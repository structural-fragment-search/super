/* This file is part of Super, a 3D vector superposition pattern-matching program.
 * This program is for unit testing the database interface
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>

#include <check.h>

#include <database.h>

#ifndef TEST_DATA_PATH
#define TEST_DATA_PATH "./test/data"
#endif


/***************CHECKS*****************/
START_TEST (create_database)
{
  struct db db;
  int err;
  struct db_header main_h;
  struct db_entry_header* h;
  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);

  fail_unless(err == ENONE,
	      "db_load_from_file reported an error");

  fail_unless(db.buffer_size == 4632,
	      "Failed to load correct size: Expected: 4632, got: %zu",
              db.buffer_size);

  fail_if(db.buffer == NULL,
	  "Database not loaded into memory");

  memcpy(&main_h, db.buffer, sizeof(struct db_header));
  fail_unless(main_h.magic == MAGIC,
	      "Magic number incorrect: 0x%x", main_h.magic);
  fail_unless(main_h.version == 1,
	      "Version number incorrect: %d", main_h.version);
  fail_unless(main_h.entry_count == 2,
	      "Number of enties in the database is incorrect: %"PRIu32,
              main_h.entry_count);
  fail_unless(main_h.metadata == 3484,
	      "Metadata offset incorrect: 0x%x", main_h.metadata);

  h = (struct db_entry_header*)((unsigned char*)db.buffer
                                + sizeof(struct db_header));

  fail_unless(h->size == 141,
	      "Size of entry does not equal 141, instead: %"PRIu16, h->size);
  fail_unless(h->base_meta_index == 0,
              "Base metadata index is not 0, instead: %d", h->base_meta_index);
  fail_unless(h->chainID == 'A',
              "Chain ID is not \'A\', instead: %c", h->chainID);
  if(strncmp(h->code, "1HHO", 4) != 0){
    fail("Entry code is incorrect");
  }

  h = (struct db_entry_header*)((unsigned char*)db.buffer
                                + sizeof(struct db_header)
                                + sizeof(struct db_entry_header)
                                + 141 * sizeof(struct db_vector));

  fail_unless(h->size == 146,
	      "Size of entry does not equal 146, instead: %"PRIu16, h->size);
  fail_unless(h->base_meta_index == 141,
              "Base metadata index is not 141, instead: %d", h->base_meta_index);
  fail_unless(h->chainID == 'B',
              "Chain ID is not \'B\', instead: %c", h->chainID);
  if(strncmp(h->code, "1HHO", 4) != 0){
    fail("Entry code is incorrect");
  }
  db_destroy(&db);
}
END_TEST

START_TEST (create_database_noexist)
{
  struct db db;
  int err;
  err = db_load_from_file("nonexistent.file", &db);
  fail_unless(err == EEXIST,
	  "Did not not report nonexistent database file");
}
END_TEST

START_TEST (create_database_badmagic)
{
  struct db db;
  int err;
  err = db_load_from_file(TEST_DATA_PATH"/not_a_super.database", &db);
  fail_unless(err == EMAGIC,
	      "Failure reported was not EMAGIC");
}
END_TEST

START_TEST (create_database_toosmall)
{
  struct db db;
  int err;
  err = db_load_from_file(TEST_DATA_PATH"/small.sdb", &db);
  fail_unless(err == EINVALID,
	      "Failure reported was not EINVALID");
}
END_TEST

START_TEST (create_database_nonreg)
{
  struct db db;
  int err;
  err = db_load_from_file(TEST_DATA_PATH, &db);
  fail_unless(err == EINVALID,
	      "Failure reported was not EINVALID");
}
END_TEST

START_TEST (create_database_noentries)
{
  struct db db;
  int err;
  err = db_load_from_file(TEST_DATA_PATH"/noentries.sdb", &db);
  fail_unless(err == EINVALID,
	      "Failure reported was not EINVALID");
}
END_TEST

START_TEST (create_database_null)
{
  struct db db;
  int err;
  err = db_load_from_file(NULL, &db);
  fail_unless(err == EMEM,
	      "Failure reported was not EMEM");

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", NULL);
  fail_unless(err == EMEM,
	      "Failure reported was not EMEM");
}
END_TEST

START_TEST (destroy_database)
{
  struct db db;
  int err;
  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);
  db_destroy(&db);
  fail_if(db.buffer != NULL);
  fail_if(db.buffer_size > 0);
}
END_TEST

START_TEST (center_single)
{
  struct db_vector src, dest;
  src.X = 5.01f;
  src.Y = -1.5f;
  src.Z = 0.83f;
  memset(&dest, 1, sizeof(struct db_vector));
  db_vector_center_initial(&src, &dest, 1);
  fail_if(fabsf(dest.X) > 0.001);
  fail_if(fabsf(dest.Y) > 0.001);
  fail_if(fabsf(dest.Z) > 0.001);
}
END_TEST

Suite*
super_suite(void)
{
  Suite* s = suite_create("Super Database");

  TCase* tc_database = tcase_create("Database");
  tcase_add_test(tc_database, create_database);
  tcase_add_test(tc_database, create_database_noexist);
  tcase_add_test(tc_database, create_database_badmagic);
  tcase_add_test(tc_database, create_database_toosmall);
  tcase_add_test(tc_database, create_database_nonreg);
  tcase_add_test(tc_database, create_database_noentries);
  tcase_add_test(tc_database, create_database_null);
  tcase_add_test(tc_database, destroy_database);
  suite_add_tcase(s, tc_database);

  TCase* tc_centering = tcase_create("Centering");
  tcase_add_test(tc_centering, center_single);
  suite_add_tcase(s, tc_centering);

  return s;
}

int main()
{
  int number_failed;
  Suite* s = super_suite();
  SRunner* sr = srunner_create(s);
  srunner_run_all(sr, CK_VERBOSE);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
