/* This file is part of Super, a 3D vector superposition pattern-matching program.
 * This program is for unit testing the utility functions
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 201109L

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#include <check.h>

#include <serrors.h>
#include <util.h>

#ifndef TEST_DATA_PATH
#define TEST_DATA_PATH "./test/data"
#endif


/***************CHECKS*****************/
START_TEST (query_path_exists)
{
  int err;
  char** query = malloc(sizeof(char*));

  const char* already_set_path = (const char*)getenv("SUPER_DB_PATH");

  fail_if(setenv("SUPER_DB_PATH", TEST_DATA_PATH, 1) != 0);

  err = get_query_path_auto(query);
  fail_if(err != ENONE);

  fail_if(strcmp(*query, TEST_DATA_PATH"/query.pdb") != 0);

  if(already_set_path){
    setenv("SUPER_DB_PATH", already_set_path, 1);
  }

  if(*query){
    free(*query);
  }
  free(query);
}
END_TEST

START_TEST (query_path_noexists)
{
  int err;
  char** query = malloc(sizeof(char*));

  const char* already_set_path = (const char*)getenv("SUPER_DB_PATH");

  fail_if(setenv("SUPER_DB_PATH", TEST_DATA_PATH"/../", 1) != 0);

  err = get_query_path_auto(query);
  fail_if(err != EEXIST);

  if(already_set_path){
    setenv("SUPER_DB_PATH", already_set_path, 1);
  }

  if(*query){
    free(*query);
  }
  free(query);
}
END_TEST

START_TEST (query_path_bad)
{
  int err;
  char** query = NULL;

  err = get_query_path_auto(query);
  fail_if(err != EMEM);
}
END_TEST

Suite*
super_suite(void)
{
  Suite* s = suite_create("Result List");

  TCase* tc_matrix = tcase_create("Results");
  tcase_add_test(tc_matrix, query_path_exists);
  tcase_add_test(tc_matrix, query_path_noexists);
  tcase_add_test(tc_matrix, query_path_bad);
  suite_add_tcase(s, tc_matrix);

  return s;
}

int main()
{
  int number_failed;
  Suite* s = super_suite();
  SRunner* sr = srunner_create(s);
  srunner_run_all(sr, CK_VERBOSE);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
