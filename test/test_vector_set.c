/* This file is part of Super. A 3D pattern-matching program.
 * This program is for unit testing the vector_set interface
 *
 * Copyright 2017, James Collier <james.collier412@gmail.com>
 *
 * Super is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Super is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Super.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>

#include <check.h>

#include <database.h>
#include <vector_set.h>

#ifndef TEST_DATA_PATH
#define TEST_DATA_PATH "./test/data"
#endif


/***************CHECKS*****************/
START_TEST (create_vector_set_contig)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 5, 0, 5);
  fail_if(err != ENONE);

  fail_if(vs.centered == NULL);
  fail_if(vs.set == NULL);
  fail_if(vs.centered == vs.set);

  fail_if(vs.base_meta_index != 0);
  fail_if(vs.vector_count != 5);
  fail_if(vs.current_vector != 0);
  fail_if(vs.first_entry != 0);
  fail_if(vs.last_entry != 1);
  fail_if(vs.current_entry != 0);

  fail_if(strncmp(vs.ID, "1HHO", 4) != 0);
  fail_if(vs.entry_size != 141);
  fail_if(vs.coil_length != 0);
  fail_if(vs.divider != 5);
  fail_if(vs.flags != 0);
  fail_if(vs.chain != 'A');

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (create_vector_set_gap)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 10, 5, 5);
  fail_if(err != ENONE);

  fail_if(vs.centered == NULL);
  fail_if(vs.set == NULL);
  fail_if(vs.centered == vs.set);

  fail_if(vs.base_meta_index != 0);
  fail_if(vs.vector_count != 10);
  fail_if(vs.current_vector != 0);
  fail_if(vs.first_entry != 0);
  fail_if(vs.last_entry != 1);
  fail_if(vs.current_entry != 0);

  fail_if(strncmp(vs.ID, "1HHO", 4) != 0);
  fail_if(vs.entry_size != 141);
  fail_if(vs.coil_length != 5);
  fail_if(vs.divider != 5);
  fail_if(vs.flags != 0);
  fail_if(vs.chain != 'A');

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (create_vector_set_entry_toosmall)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 146, 0, 146);
  fail_if(err != ENONE);

  fail_if(vs.centered == NULL);
  fail_if(vs.set == NULL);
  fail_if(vs.centered == vs.set);

  fail_if(vs.base_meta_index != 141);
  fail_if(vs.vector_count != 146);
  fail_if(vs.current_vector != 0);
  fail_if(vs.first_entry != 0);
  fail_if(vs.last_entry != 1);
  fail_if(vs.current_entry != 1);

  fail_if(strncmp(vs.ID, "1HHO", 4) != 0);
  fail_if(vs.entry_size != 146);
  fail_if(vs.coil_length != 0);
  fail_if(vs.divider != 146);
  fail_if(vs.flags != 0);
  fail_if(vs.chain != 'B');

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (create_vector_set_all_entry_toosmall)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 147, 0, 147);
  fail_if(err != EOOB);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (destroy_vector_set)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 5, 0, 5);
  fail_if(err != ENONE);

  vector_set_destroy(&vs);
  fail_if(vs.centered != NULL);
  fail_if(vs.set != NULL);

  fail_if(vs.base_meta_index != 0);
  fail_if(vs.vector_count != 0);
  fail_if(vs.current_vector != 0);
  fail_if(vs.first_entry != 0);
  fail_if(vs.last_entry != 0);
  fail_if(vs.current_entry != 0);
  fail_if(vs.entry_size != 0);
  fail_if(vs.coil_length != 0);
  fail_if(vs.divider != 0);
  fail_if(vs.flags != VECTOR_SET_DESTROYED);
  
  db_destroy(&db);
}
END_TEST

START_TEST (proceed_vector_set_contig)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 140, 0, 140);
  fail_if(err != ENONE);

  err = vector_set_proceed(&vs);
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 1);
  fail_if(vs.current_entry != 0);

  err = vector_set_proceed(&vs);
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 0);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //141
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 1);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //142
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 2);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //143
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 3);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //144
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 4);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //145
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 5);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //146
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 6);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs);
  fail_if(err != EOOB);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (proceed_vector_set_gap)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 140, 5, 5);
  fail_if(err != ENONE);

  err = vector_set_proceed(&vs);
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 1);
  fail_if(vs.current_entry != 0);

  err = vector_set_proceed(&vs);
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 0);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //141
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 1);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //142
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 2);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //143
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 3);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //144
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 4);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //145
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 5);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //146
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 6);
  fail_if(vs.current_entry != 1);

  err = vector_set_proceed(&vs); //OOB
  fail_if(err != EOOB);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (seek_vector_set)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 140, 0, 140);
  fail_if(err != ENONE);

  err = vector_set_seek(&vs, 1);
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 0);
  fail_if(vs.current_entry != 1);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (seek_vector_set_past_end)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 140, 0, 140);
  fail_if(err != ENONE);

  err = vector_set_seek(&vs, 2);
  fail_if(err != EOOB);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (seek_vector_set_earlier)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 140, 0, 140);
  fail_if(err != ENONE);

  err = vector_set_seek(&vs, 1);
  fail_if(err != ENONE);

  err = vector_set_seek(&vs, 0);
  fail_if(err != EOOB);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (seek_vector_set_toobig)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/largetestdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 188, 0, 188);
  fail_if(err != ENONE);
  fail_if(vs.current_entry != 3);

  err = vector_set_seek(&vs, 6);
  fail_if(err != ENONE);
  fail_if(vs.current_vector != 0);
  fail_if(vs.current_entry != 8);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (clone_vector_set_contig)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 5, 0, 5);
  fail_if(err != ENONE);

  struct vector_set* clone = vector_set_clone(&vs);
  fail_if(clone == NULL);

  fail_if(clone->centered == NULL);
  fail_if(clone->set == NULL);
  fail_if(clone->centered == clone->set);
  fail_if(clone->set != vs.set);

  fail_if(clone->base_meta_index != 0);
  fail_if(clone->vector_count != 5);
  fail_if(clone->current_vector != 0);
  fail_if(clone->first_entry != 0);
  fail_if(clone->last_entry != 1);
  fail_if(clone->current_entry != 0);

  fail_if(strncmp(clone->ID, "1HHO", 4) != 0);
  fail_if(clone->entry_size != 141);
  fail_if(clone->coil_length != 0);
  fail_if(clone->divider != 5);
  fail_if((clone->flags & VECTOR_SET_CLONED) != VECTOR_SET_CLONED);
  fail_if(clone->chain != 'A');

  vector_set_destroy(clone);
  free(clone);
  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (clone_vector_set_gap)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 10, 5, 5);
  fail_if(err != ENONE);

  struct vector_set* clone = vector_set_clone(&vs);
  fail_if(clone == NULL);

  fail_if(clone->centered == NULL);
  fail_if(clone->set == NULL);
  fail_if(clone->centered == clone->set);
  fail_if(clone->set != clone->set);

  fail_if(clone->base_meta_index != 0);
  fail_if(clone->vector_count != 10);
  fail_if(clone->current_vector != 0);
  fail_if(clone->first_entry != 0);
  fail_if(clone->last_entry != 1);
  fail_if(clone->current_entry != 0);

  fail_if(strncmp(clone->ID, "1HHO", 4) != 0);
  fail_if(clone->entry_size != 141);
  fail_if(clone->coil_length != 5);
  fail_if(clone->divider != 5);
  fail_if((clone->flags & VECTOR_SET_CLONED) != VECTOR_SET_CLONED);
  fail_if(clone->chain != 'A');

  vector_set_destroy(clone);
  free(clone);
  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (clone_vector_set_destroyed)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 10, 5, 5);
  fail_if(err != ENONE);

  vector_set_destroy(&vs);

  struct vector_set* clone = vector_set_clone(&vs);
  fail_if(clone != NULL);

  db_destroy(&db);
}
END_TEST

START_TEST (clone_vector_set_proceed)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 140, 0, 140);
  fail_if(err != ENONE);

  struct vector_set* clone = vector_set_clone(&vs);
  fail_if(clone == NULL);

  err = vector_set_proceed(clone);
  fail_if(err != ENONE);
  fail_if(clone->current_vector != 1);
  fail_if(clone->current_entry != 0);

  err = vector_set_proceed(&vs);
  fail_if(err != ENONE);
  fail_if(clone->current_vector != vs.current_vector);
  fail_if(clone->current_entry != vs.current_entry);

  err = vector_set_proceed(clone);
  fail_if(err != ENONE);
  fail_if(clone->current_vector != 0);
  fail_if(clone->current_entry != 1);

  err = vector_set_proceed(&vs);
  fail_if(err != ENONE);
  fail_if(clone->current_vector != vs.current_vector);
  fail_if(clone->current_entry != vs.current_entry);

  err = vector_set_proceed(clone); //141
  fail_if(err != ENONE);
  fail_if(clone->current_vector != 1);
  fail_if(clone->current_entry != 1);

  err = vector_set_proceed(clone); //142
  fail_if(err != ENONE);
  fail_if(clone->current_vector != 2);
  fail_if(clone->current_entry != 1);

  err = vector_set_proceed(clone); //143
  fail_if(err != ENONE);
  fail_if(clone->current_vector != 3);
  fail_if(clone->current_entry != 1);

  err = vector_set_proceed(clone); //144
  fail_if(err != ENONE);
  fail_if(clone->current_vector != 4);
  fail_if(clone->current_entry != 1);

  err = vector_set_proceed(clone); //145
  fail_if(err != ENONE);
  fail_if(clone->current_vector != 5);
  fail_if(clone->current_entry != 1);

  err = vector_set_proceed(clone); //146
  fail_if(err != ENONE);
  fail_if(clone->current_vector != 6);
  fail_if(clone->current_entry != 1);

  err = vector_set_proceed(clone); //OOB
  fail_if(err != EOOB);

  vector_set_destroy(clone);
  free(clone);
  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (restrict_vector_set_small)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 5, 0, 5);
  fail_if(err != ENONE);

  err = vector_set_restrict(&vs, 0, 0);
  fail_if(err != ENONE);
  fail_if(vs.first_entry != 0);
  fail_if(vs.last_entry != 0);
  fail_if(vs.current_entry != 0);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (restrict_vector_set_large)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/largetestdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 5, 0, 5);
  fail_if(err != ENONE);

  err = vector_set_restrict(&vs, 5, 16);
  fail_if(err != ENONE);
  fail_if(vs.first_entry != 5);
  fail_if(vs.last_entry != 16);
  fail_if(vs.current_entry != 5);
  fail_if(vs.current_vector != 0);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (restrict_vector_set_oob_end)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/largetestdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 5, 0, 5);
  fail_if(err != ENONE);

  err = vector_set_restrict(&vs, 5, 41);
  fail_if(err != EOOB);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (restrict_vector_set_flflipped)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/largetestdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 5, 0, 5);
  fail_if(err != ENONE);

  err = vector_set_restrict(&vs, 5, 3);
  fail_if(err != EOOB);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (restrict_vector_set_past_first)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 5, 0, 5);
  fail_if(err != ENONE);

  err = vector_set_seek(&vs, 1);
  fail_if(err != ENONE);

  err = vector_set_restrict(&vs, 0, 1);
  fail_if(err != ENONE);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (restrict_vector_set_toobig)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/largetestdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 5, 0, 5);
  fail_if(err != ENONE);

  err = vector_set_seek(&vs, 10);
  fail_if(err != ENONE);

  err = vector_set_restrict(&vs, 0, 1);
  fail_if(err != EOOB);

  vector_set_destroy(&vs);
  db_destroy(&db);
}
END_TEST

START_TEST (center_vector_set_contig)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 5, 0, 5);
  fail_if(err != ENONE);

  float X = 0.0f, Y = 0.0f, Z = 0.0f;
  for(unsigned i = 0; i < 5; i++){
    X += vs.centered[i].X;
    Y += vs.centered[i].Y;
    Z += vs.centered[i].Z;
  }

  fail_if(fabsf(X) > 0.0001);
  fail_if(fabsf(Y) > 0.0001);
  fail_if(fabsf(Z) > 0.0001);
}
END_TEST

START_TEST (center_vector_set_gap)
{
  int err;
  struct vector_set vs;
  struct db db;

  err = db_load_from_file(TEST_DATA_PATH"/testdatabase.sdb", &db);
  fail_if(err != ENONE);

  err = vector_set_create(&vs, &db, 10, 5, 5);
  fail_if(err != ENONE);

  float X = 0.0f, Y = 0.0f, Z = 0.0f;
  for(unsigned i = 0; i < 10; i++){
    X += vs.centered[i].X;
    Y += vs.centered[i].Y;
    Z += vs.centered[i].Z;
  }

  fail_if(fabsf(X) > 0.0001);
  fail_if(fabsf(Y) > 0.0001);
  fail_if(fabsf(Z) > 0.0001);
}
END_TEST

Suite*
super_suite(void)
{
  Suite* s = suite_create("Super Vector Set");

  TCase* tc_vector_set = tcase_create("VectorSet");
  tcase_add_test(tc_vector_set, create_vector_set_contig);
  tcase_add_test(tc_vector_set, create_vector_set_gap);
  tcase_add_test(tc_vector_set, create_vector_set_entry_toosmall);
  tcase_add_test(tc_vector_set, create_vector_set_all_entry_toosmall);
  tcase_add_test(tc_vector_set, destroy_vector_set);
  tcase_add_test(tc_vector_set, proceed_vector_set_contig);
  tcase_add_test(tc_vector_set, proceed_vector_set_gap);
  tcase_add_test(tc_vector_set, seek_vector_set);
  tcase_add_test(tc_vector_set, seek_vector_set_past_end);
  tcase_add_test(tc_vector_set, seek_vector_set_earlier);
  tcase_add_test(tc_vector_set, seek_vector_set_toobig);
  tcase_add_test(tc_vector_set, clone_vector_set_contig);
  tcase_add_test(tc_vector_set, clone_vector_set_gap);
  tcase_add_test(tc_vector_set, clone_vector_set_destroyed);
  tcase_add_test(tc_vector_set, clone_vector_set_proceed);
  tcase_add_test(tc_vector_set, restrict_vector_set_small);
  tcase_add_test(tc_vector_set, restrict_vector_set_large);
  tcase_add_test(tc_vector_set, restrict_vector_set_oob_end);
  tcase_add_test(tc_vector_set, restrict_vector_set_flflipped);
  tcase_add_test(tc_vector_set, restrict_vector_set_past_first);
  tcase_add_test(tc_vector_set, restrict_vector_set_toobig);
  tcase_add_test(tc_vector_set, center_vector_set_contig);
  tcase_add_test(tc_vector_set, center_vector_set_gap);
  suite_add_tcase(s, tc_vector_set);

  return s;
}

int main()
{
  int number_failed;
  Suite* s = super_suite();
  SRunner* sr = srunner_create(s);
  srunner_run_all(sr, CK_VERBOSE);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
